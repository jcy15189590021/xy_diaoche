package com.ruoyi.service.Dto;

import com.ruoyi.common.mongValue.SysUserCard;

public class UserCompanyAddInDto {

    private SysUserCard sysUserCard = new SysUserCard();

    public SysUserCard getSysUserCard() {
        return sysUserCard;
    }

    public void setSysUserCard(SysUserCard sysUserCard) {
        this.sysUserCard = sysUserCard;
    }
}
