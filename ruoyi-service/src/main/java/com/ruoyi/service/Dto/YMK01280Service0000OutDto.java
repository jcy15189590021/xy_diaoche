package com.ruoyi.service.Dto;

import com.ruoyi.common.mongValue.SveCranes;
import lombok.Data;

import java.util.List;

@Data
public class YMK01280Service0000OutDto {

    private List<SveCranes> sveCranes;
}
