package com.ruoyi.service.Dto;

import com.ruoyi.common.mongValue.CraneMongoValue;
import lombok.Data;

import java.util.List;
import java.util.Map;

@Data
public class YMK01240Service0040OutDto {

    /**
     * 起重机类型
     */
    private Map<String, List<CraneMongoValue>> craneInfo1;

    /**
     * 起重机具体信息
     */
    private Map<Long, CraneMongoValue> craneInfo2;

}
