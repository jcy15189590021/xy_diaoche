package com.ruoyi.service.Dto;

import com.ruoyi.common.mongValue.SysUserCard;
import com.ruoyi.common.mongValue.SysUserStatus;
import com.ruoyi.common.value.ChartValue;
import com.ruoyi.common.value.PanelGroup;
import lombok.Data;

import java.util.List;

@Data
public class YMK01100Service0000OutDto {

    private PanelGroup panelGroup;

    private SysUserStatus userStatus;

    /*起重机类型折线图*/
    private ChartValue craneChart;

    /*账单成交数折线图*/
    private ChartValue billCountChart;

    /*账单成交额折线图*/
    private ChartValue billAmountChart;
}
