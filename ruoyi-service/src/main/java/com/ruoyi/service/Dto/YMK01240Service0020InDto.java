package com.ruoyi.service.Dto;

import com.ruoyi.common.mongValue.SveCranes;
import lombok.Data;

@Data
public class YMK01240Service0020InDto {

    private String remark;

    private SveCranes sveCrane;

}
