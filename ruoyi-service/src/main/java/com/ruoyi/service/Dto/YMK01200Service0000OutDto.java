package com.ruoyi.service.Dto;

import com.ruoyi.common.mongValue.OrderAddValue;
import com.ruoyi.common.mongValue.SysUserStatus;
import lombok.Data;

import java.util.List;

@Data
public class YMK01200Service0000OutDto {

    private List<OrderAddValue> orderValues;

    private SysUserStatus userStatus;
}
