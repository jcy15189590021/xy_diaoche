package com.ruoyi.service.Dto;

import com.ruoyi.common.mongValue.CraneMongoValue;
import lombok.Data;

@Data
public class YMK01230Service0030InDto {

    private CraneMongoValue craneMongoValue;
}
