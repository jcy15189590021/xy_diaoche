package com.ruoyi.service.Dto;

import com.ruoyi.common.mongValue.SysUserCard;
import lombok.Data;

@Data
public class YMK01160Service0000OutDto {
    /* 认证状态*/
    private String isCompany;

    /* 实名认证信息 */
    private SysUserCard sysUserCard;

    /* 审核留言 */
    private String remark;
}
