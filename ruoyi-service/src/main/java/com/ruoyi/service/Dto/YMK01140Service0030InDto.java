package com.ruoyi.service.Dto;

import com.ruoyi.common.mongValue.SysUserCard;
import lombok.Data;

@Data
public class YMK01140Service0030InDto {

    private SysUserCard sysUserCard;

    private String remark;
}
