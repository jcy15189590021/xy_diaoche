package com.ruoyi.service.controller;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.PageUtil;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.utils.value.PageList;
import com.ruoyi.service.Dto.*;
import com.ruoyi.common.mongValue.SveCranes;
import com.ruoyi.service.service.YMK01240Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 起重机信息Controller
 *
 * @author ruoyi
 * @date 2024-03-29
 */
@RestController
@RequestMapping("/service/cranes")
public class YMK01240Controller extends BaseController {

    @Autowired
    private YMK01240Service ymk01240Service;

    /**
     * 查询起重机信息列表
     */
    @GetMapping("/0000/{current}/{size}")
    public AjaxResult ymk01240_0000(SveCranes sveCranes,
                                    @PathVariable String current,
                                    @PathVariable String size)
    {
        YMK01240Service0000InDto inDto = new YMK01240Service0000InDto();
        inDto.setSveCrane(sveCranes);
        logger.info(sveCranes.toString());
        List<SveCranes> list = ymk01240Service.ymk01240_0000(inDto);
        PageUtil pageUtil = new PageUtil();
        PageList<SveCranes> cranePageList =  pageUtil.listConverToPage(list,Integer.valueOf(size),Integer.valueOf(current));
        return success().put("total", cranePageList.getTotal())
                .put("current",cranePageList.getCurrent())
                .put("pages",cranePageList.getPages())
                .put("size",cranePageList.getSize())
                .put("records",cranePageList.getRecords());
    }

    /**
     * 修改起重机信息
     */
    @Log(title = "起重机信息", businessType = BusinessType.UPDATE)
    @PostMapping("/0010")
    public AjaxResult ymk01240_0010(@RequestBody SveCranes sveCranes)
    {
        YMK01240Service0010InDto inDto = new YMK01240Service0010InDto();
        inDto.setSveCrane(sveCranes);
        return toAjax(ymk01240Service.ymk01240_0010(inDto));
    }

    /**
     * 审核起重机
     */
    @Log(title = "起重机信息", businessType = BusinessType.UPDATE)
    @PostMapping("/0020/{remark}")
    public AjaxResult ymk01240_0020(@RequestBody SveCranes sveCrane, @PathVariable String remark)
    {
        YMK01240Service0020InDto inDto = new YMK01240Service0020InDto();
        inDto.setRemark(remark);
        inDto.setSveCrane(sveCrane);
        return toAjax(ymk01240Service.ymk01240_0020(inDto));
    }

    /**
     * 获取起重机信息详细信息
     */
    @GetMapping(value = "/0030/{craneId}")
    public AjaxResult ymk01240_0030(@PathVariable("craneId") Long craneId)
    {
        YMK01240Service0030OutDto outDto = ymk01240Service.ymk01240_0030(craneId);
        return success().put("row",outDto.getSveCrane())
                .put("remark",outDto.getRemark())
                .put("craneInfo",outDto.getCraneInfo1())
                .put("craneInfoOption",outDto.getCraneInfo2());
    }

    /**
     * 起重机信息列表初期化
     */
    @GetMapping("/0040")
    public AjaxResult ymk01240_0040()
    {
        YMK01240Service0040OutDto outDto = ymk01240Service.ymk01240_0040();
        return success().put("craneInfo",outDto.getCraneInfo1()).put("craneInfoOptionMap",outDto.getCraneInfo2());
    }

    /**
     * 新增起重机信息
     */
    @Log(title = "起重机信息", businessType = BusinessType.INSERT)
    @PostMapping("/0050")
    public AjaxResult ymk01240_0050(@RequestBody SveCranes sveCranes)
    {
        YMK01240Service0050InDto indto = new YMK01240Service0050InDto();
        indto.setSveCrane(sveCranes);

        return toAjax(ymk01240Service.ymk01240_0050(indto));
    }

    /**
     * 删除起重机信息
     */
    @Log(title = "起重机信息", businessType = BusinessType.DELETE)
    @PostMapping("/0060/{craneId}")
    public AjaxResult ymk01240_0060(@PathVariable Long craneId)
    {
        return toAjax(ymk01240Service.ymk01240_0060(craneId));
    }
}
