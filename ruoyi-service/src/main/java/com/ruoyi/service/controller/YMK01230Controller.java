package com.ruoyi.service.controller;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.PageUtil;
import com.ruoyi.common.utils.value.PageList;
import com.ruoyi.common.mongValue.SveCranes;
import com.ruoyi.common.mongValue.CraneMongoValue;
import com.ruoyi.service.Dto.YMK01230Service0030InDto;
import com.ruoyi.service.Dto.YMK01230Service0040InDto;
import com.ruoyi.service.service.YMK01230Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 起重机类型Controller
 *
 * @date 2024-03-29
 */
@RestController
@RequestMapping("/service/craneInfo")
public class YMK01230Controller extends BaseController {

    @Autowired
    private YMK01230Service ymk01230Service;

    /**
     * 查询起重机信息列表
     */
    @GetMapping("/0000/{current}/{size}")
    public AjaxResult ymk01230_0000(CraneMongoValue craneMongoValue,
                                    @PathVariable String current,
                                    @PathVariable String size)
    {
        List<CraneMongoValue> list = ymk01230Service.ymk01230_0000(craneMongoValue);
        PageUtil pageUtil = new PageUtil();
        PageList<CraneMongoValue> cranePageList =  pageUtil.listConverToPage(list,Integer.valueOf(size),Integer.valueOf(current));
        return success().put("total", cranePageList.getTotal())
                .put("current",cranePageList.getCurrent())
                .put("pages",cranePageList.getPages())
                .put("size",cranePageList.getSize())
                .put("records",cranePageList.getRecords());
    }

    /**
     * 获取起重机信息详细信息
     */
    @PostMapping(value = "/0010/{craneId}")
    public AjaxResult ymk01230_0010(@PathVariable("craneId") Long craneId)
    {
        return success(ymk01230Service.ymk01230_0010(craneId));
    }

    /**
     * 新增起重机信息
     */
    @Log(title = "起重机信息", businessType = BusinessType.INSERT)
    @PostMapping(value = "/0020")
    public AjaxResult ymk01230_0020(@RequestBody(required = false) CraneMongoValue craneMongoValue)
    {

        return toAjax(ymk01230Service.ymk01230_0020(craneMongoValue));
    }

    /**
     * 修改起重机信息
     */
    @Log(title = "起重机信息", businessType = BusinessType.UPDATE)
    @PostMapping(value = "/0030")
    public AjaxResult ymk01230_0030(@RequestBody CraneMongoValue craneMongoValue)
    {
        YMK01230Service0030InDto inDto = new YMK01230Service0030InDto();
        inDto.setCraneMongoValue(craneMongoValue);
        return toAjax(ymk01230Service.ymk01230_0030(inDto));
    }

    /**
     * 删除起重机信息
     */
    @Log(title = "起重机信息", businessType = BusinessType.DELETE)
    @PostMapping(value = "/0040/{craneId}")
    public AjaxResult ymk01230_0040(@PathVariable Long craneId)
    {
        logger.info(craneId.toString());
        YMK01230Service0040InDto inDto = new YMK01230Service0040InDto();
        inDto.setId(craneId);
        return toAjax(ymk01230Service.ymk01230_0040(inDto));
    }
}
