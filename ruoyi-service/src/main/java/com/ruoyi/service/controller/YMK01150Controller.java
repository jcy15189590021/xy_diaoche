package com.ruoyi.service.controller;

import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.mongValue.SysUserCard;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.service.Dto.YMK01150Service0000OutDto;
import com.ruoyi.service.Dto.YMK01150Service0010InDto;
import com.ruoyi.service.service.YMK01150Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 实名认证Controller
 */
@RestController
@RequestMapping("/system/userauth")
public class YMK01150Controller extends BaseController {

    @Autowired
    private YMK01150Service ymk01150Service;

    /**
     * 查询实名认证信息
     */
    @GetMapping("/0000")
    public AjaxResult ymk01150_0000()
    {
        YMK01150Service0000OutDto outDto = ymk01150Service.ymk01150_0000();
        return success().put("status",outDto.getIsReal()).put("remark", outDto.getRemark()).put("userCard", outDto.getSysUserCard());
    }

    /**
     * 实名认证
     * @param sysUserCard
     * @return
     */
    @PostMapping("/0010")
    public AjaxResult ymk01150_0010(@RequestBody(required = false) SysUserCard sysUserCard) {
        SysUser user = SecurityUtils.getLoginUser().getUser();
        sysUserCard.setUserId(user.getUserId());
        YMK01150Service0010InDto inDto = new YMK01150Service0010InDto();
        inDto.setSysUserCard(sysUserCard);
        return toAjax(ymk01150Service.ymk01150_0010(inDto));
    }
}
