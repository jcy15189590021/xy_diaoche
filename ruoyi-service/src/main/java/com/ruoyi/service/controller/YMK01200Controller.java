package com.ruoyi.service.controller;

import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.PageUtil;
import com.ruoyi.common.utils.value.PageList;
import com.ruoyi.service.Dto.*;
import com.ruoyi.service.service.YMK01200Service;
import com.ruoyi.common.mongValue.OrderAddValue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 订单信息Controller
 *
 * @author ruoyi
 * @date 2024-03-29
 */
@RestController
@RequestMapping("/service/order")
public class YMK01200Controller extends BaseController {

    @Autowired
    private YMK01200Service ymk01200Service;

    /**
     * 查询起重机信息列表
     */
    @GetMapping("/0000/{current}/{size}")
    public AjaxResult ymk01200_0000(OrderAddValue orderAddValue,
                                    @PathVariable String current,
                                    @PathVariable String size)
    {
        YMK01200Service0000OutDto outDto = ymk01200Service.ymk01200_0000();
        PageUtil pageUtil = new PageUtil();
        PageList<OrderAddValue> orderPageList =  pageUtil.listConverToPage(outDto.getOrderValues(),Integer.valueOf(size),Integer.valueOf(current));
        return success().put("total", orderPageList.getTotal())
                .put("current",orderPageList.getCurrent())
                .put("pages",orderPageList.getPages())
                .put("size",orderPageList.getSize())
                .put("records",orderPageList.getRecords())
                .put("userStatus", outDto.getUserStatus());
    }


    /**
     * 订单生成之前信息查询
     * @param id
     * @return
     */
    @PostMapping(value = "/0010/{id}")
    public AjaxResult ymk01200_0010(@PathVariable("id") Long id) {
        YMK01200Service0010InDto inDto = new YMK01200Service0010InDto();
        inDto.setId(Long.valueOf(id));
        YMK01200Service0010OutDto outDto = ymk01200Service.ymk01280_0010(inDto);
        return success().put("formData",outDto.getOrderAddValue());
    }

    /**
     * 修改订单
     * @param orderAddValue
     * @return
     */
    @PostMapping(value = "/0020")
    public AjaxResult ymk01200_0020(@RequestBody(required = false) OrderAddValue orderAddValue) {
        YMK01200Service0020InDto inDto = new YMK01200Service0020InDto();
        inDto.setOrderAddValue(orderAddValue);

        return toAjax(ymk01200Service.ymk01200_0020(inDto));
    }

    /**
     * 撤销订单
     * @param orderAddValue
     * @return
     */
    @PostMapping(value = "/0030")
    public AjaxResult ymk01200_0030(@RequestBody(required = false) OrderAddValue orderAddValue) {
        YMK01200Service0030InDto inDto = new YMK01200Service0030InDto();
        inDto.setOrderAddValue(orderAddValue);

        return toAjax(ymk01200Service.ymk01200_0030(inDto));
    }

    /**
     * 撤销订单
     * @param orderAddValue
     * @return
     */
    @PostMapping(value = "/0040")
    public AjaxResult ymk01200_0040(@RequestBody(required = false) OrderAddValue orderAddValue) {
        YMK01200Service0040InDto inDto = new YMK01200Service0040InDto();
        inDto.setOrderAddValue(orderAddValue);
        return toAjax(ymk01200Service.ymk01200_0040(inDto));
    }

    /**
     * 接受订单
     * @param orderAddValue
     * @return
     */
    @PostMapping(value = "/0050")
    public AjaxResult ymk01200_0050(@RequestBody(required = false) OrderAddValue orderAddValue) {
        YMK01200Service0050InDto inDto = new YMK01200Service0050InDto();
        inDto.setOrderAddValue(orderAddValue);
        return toAjax(ymk01200Service.ymk01200_0050(inDto));
    }

    /**
     * 不接受订单
     * @param orderAddValue
     * @return
     */
    @PostMapping(value = "/0060")
    public AjaxResult ymk01200_0060(@RequestBody(required = false) OrderAddValue orderAddValue) {
        YMK01200Service0060InDto inDto = new YMK01200Service0060InDto();
        inDto.setOrderAddValue(orderAddValue);
        return toAjax(ymk01200Service.ymk01200_0060(inDto));
    }

    /**
     * 用户已完成订单
     * @param orderAddValue
     * @return
     */
    @PostMapping(value = "/0070")
    public AjaxResult ymk01200_0070(@RequestBody(required = false) OrderAddValue orderAddValue) {
        YMK01200Service0070InDto inDto = new YMK01200Service0070InDto();
        inDto.setOrderAddValue(orderAddValue);
        return toAjax(ymk01200Service.ymk01200_0070(inDto));
    }

    /**
     * 商家已完成订单
     * @param orderAddValue
     * @return
     */
    @PostMapping(value = "/0080")
    public AjaxResult ymk01200_0080(@RequestBody(required = false) OrderAddValue orderAddValue) {
        YMK01200Service0080InDto inDto = new YMK01200Service0080InDto();
        inDto.setOrderAddValue(orderAddValue);
        return toAjax(ymk01200Service.ymk01200_0080(inDto));
    }
}
