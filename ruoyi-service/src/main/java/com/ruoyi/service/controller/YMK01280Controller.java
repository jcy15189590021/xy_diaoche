package com.ruoyi.service.controller;

import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.PageUtil;
import com.ruoyi.common.utils.value.PageList;
import com.ruoyi.service.Dto.YMK01280Service0000OutDto;
import com.ruoyi.service.Dto.YMK01280Service0010InDto;
import com.ruoyi.service.Dto.YMK01280Service0010OutDto;
import com.ruoyi.service.Dto.YMK01280Service0020InDto;
import com.ruoyi.common.mongValue.SveCranes;
import com.ruoyi.service.service.YMK01280Service;
import com.ruoyi.common.mongValue.OrderAddValue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 起重机租赁Controller
 *
 * @author ruoyi
 * @date 2024-03-29
 */
@RestController
@RequestMapping("/service/lease")
public class YMK01280Controller extends BaseController {

    @Autowired
    private YMK01280Service ymk01280Service;

    /**
     * 查询起重机信息列表
     */
    @GetMapping("/0000/{current}/{size}")
    public AjaxResult ymk01280_0000(SveCranes sveCranes,
                                    @PathVariable String current,
                                    @PathVariable String size)
    {
        logger.info(sveCranes.toString());
        YMK01280Service0000OutDto outDto = ymk01280Service.ymk01280_0000(sveCranes);
        PageUtil pageUtil = new PageUtil();
        PageList<SveCranes> cranePageList =  pageUtil.listConverToPage(outDto.getSveCranes(),Integer.valueOf(size),Integer.valueOf(current));
        return success().put("total", cranePageList.getTotal())
                .put("current",cranePageList.getCurrent())
                .put("pages",cranePageList.getPages())
                .put("size",cranePageList.getSize())
                .put("records",cranePageList.getRecords());
    }

    /**
     * 订单生成之前信息查询
     * @param id
     * @return
     */
    @PostMapping(value = "/0010/{id}")
    public AjaxResult ymk01280_0010(@PathVariable("id") Long id) {
        YMK01280Service0010InDto inDto = new YMK01280Service0010InDto();
        inDto.setId(Long.valueOf(id));
        YMK01280Service0010OutDto outDto = ymk01280Service.ymk01280_0010(inDto);
        return success().put("formData",outDto.getOrderAddValue());
    }

    /**
     * 添加订单
     * @param orderAddValue
     * @return
     */
    @PostMapping(value = "/0020")
    public AjaxResult ymk01280_0020(@RequestBody(required = false) OrderAddValue orderAddValue) {
        logger.info(orderAddValue.toString());
        YMK01280Service0020InDto inDto = new YMK01280Service0020InDto();
        inDto.setOrderAddValue(orderAddValue);
        ymk01280Service.ymk01280_0020(inDto);
        return toAjax(1);
    }

}
