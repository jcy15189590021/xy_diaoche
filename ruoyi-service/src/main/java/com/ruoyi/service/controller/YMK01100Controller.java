package com.ruoyi.service.controller;

import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.service.Dto.YMK01100Service0000OutDto;
import com.ruoyi.service.service.YMK01100Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static com.ruoyi.common.core.domain.AjaxResult.success;

@RestController
@RequestMapping("/system/dashboard")
public class YMK01100Controller {

    @Autowired
    private YMK01100Service ymk01100Service;

    /**
     * 查询起重机信息列表
     */
    @GetMapping("/0000")
    public AjaxResult ymk01100_0000()
    {
//        YMK01140Service0000OutDto outDto = ymk01140Service.ymk01140_0000(sysUserCard);
        YMK01100Service0000OutDto outDto = ymk01100Service.ymk01100_0000();
        return success()
                .put("panelGroup", outDto.getPanelGroup())
                .put("billCountChart",outDto.getBillCountChart())
                .put("billAmountChart",outDto.getBillAmountChart())
                .put("craneChart",outDto.getCraneChart());
    }

}
