package com.ruoyi.service.controller;

import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.mongValue.SysUserCard;
import com.ruoyi.common.utils.PageUtil;
import com.ruoyi.common.utils.value.PageList;
import com.ruoyi.service.Dto.YMK01140Service0000OutDto;
import com.ruoyi.service.Dto.YMK01140Service0030InDto;
import com.ruoyi.service.service.YMK01140Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 资格认证
 */
@RestController
@RequestMapping("/system/qualify")
public class YMK01140Controller extends BaseController {

    @Autowired
    private YMK01140Service ymk01140Service;

    /**
     * 查询资格认证信息列表
     */
    @GetMapping("/0000/{current}/{size}")
    public AjaxResult ymk01140_0000(SysUserCard sysUserCard,
                                    @PathVariable String current,
                                    @PathVariable String size)
    {
        YMK01140Service0000OutDto outDto = ymk01140Service.ymk01140_0000(sysUserCard);
        PageUtil pageUtil = new PageUtil();
        PageList<SysUserCard> cranePageList =  pageUtil.listConverToPage(outDto.getSysUserCards(),Integer.valueOf(size),Integer.valueOf(current));
        return success().put("total", cranePageList.getTotal())
                .put("current",cranePageList.getCurrent())
                .put("pages",cranePageList.getPages())
                .put("size",cranePageList.getSize())
                .put("records",cranePageList.getRecords());
    }

    /**
     * 获取证件认证详细信息
     */
    @GetMapping(value = "/0010/{userId}")
    public AjaxResult ymk01140_0010(@PathVariable("userId") Long userId)
    {
        return success(ymk01140Service.ymk01140_0010(userId));
    }

    /**
     * 审核通过
     */
    @PostMapping("/0020")
    public AjaxResult ymk01140_0020(@RequestBody SysUserCard sysUserCard)
    {
        return toAjax(ymk01140Service.ymk01140_0020(sysUserCard));
    }


    /**
     * 审核不通过
     */
    @PostMapping("/0030/{remark}")
    public AjaxResult ymk01140_0030(@RequestBody SysUserCard sysUserCard, @PathVariable String remark)
    {
        YMK01140Service0030InDto inDto = new YMK01140Service0030InDto();
        inDto.setSysUserCard(sysUserCard);
        inDto.setRemark(remark);
        return toAjax(ymk01140Service.ymk01140_0030(inDto));
    }
}
