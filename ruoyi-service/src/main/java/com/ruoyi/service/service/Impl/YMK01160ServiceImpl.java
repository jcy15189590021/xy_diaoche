package com.ruoyi.service.service.Impl;

import com.ruoyi.common.constant.MongoConstant;
import com.ruoyi.common.core.Service.BaseServiceImpl;
import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.mongValue.QualifyLog;
import com.ruoyi.common.mongValue.SysUserCard;
import com.ruoyi.common.mongValue.SysUserStatus;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.service.Dto.YMK01150Service0000OutDto;
import com.ruoyi.service.Dto.YMK01160Service0000OutDto;
import com.ruoyi.service.Dto.YMK01160Service0010InDto;
import com.ruoyi.service.service.YMK01160Service;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

/**
 * 公司认证业务处理
 */
@Service
public class YMK01160ServiceImpl extends BaseServiceImpl implements YMK01160Service {
    @Override
    public YMK01160Service0000OutDto ymk01160_0000() {
        YMK01160Service0000OutDto outDto = new YMK01160Service0000OutDto();
        SysUser user = SecurityUtils.getLoginUser().getUser();
        SysUserStatus sysUserStatus = new SysUserStatus();
        Query query = new Query();
        query.addCriteria(Criteria.where("userId").is(user.getUserId()));
        sysUserStatus = mongoTemplate.findOne(query, SysUserStatus.class, MongoConstant.MONGODB_NAME_USERSTATUS);
        String remark = StringUtils.EMPTY;
        SysUserCard sysUserCard = new SysUserCard();
        sysUserCard.setCardType("6");
        if (sysUserStatus.getIsCompany().equals("0")) {
            outDto.setSysUserCard(sysUserCard);
        } else {
            sysUserCard = mongoTemplate.findOne(new Query(Criteria.where("userId").is(user.getUserId())
                            .where("cardType").is("6")),
                    SysUserCard.class, MongoConstant.MONGODB_NAME_USERCARD);
            outDto.setSysUserCard(sysUserCard);
            if (sysUserStatus.getIsReal().equals("3")) {
                QualifyLog qualifyLog = mongoTemplate.findOne(new Query(Criteria.where("id").is(sysUserCard.getQualifyLogId())),
                        QualifyLog.class, MongoConstant.MONGODB_NAME_QUALIFYLOG);
                remark = qualifyLog.getRemark();
            }
        }
        outDto.setIsCompany(sysUserStatus.getIsCompany());
        outDto.setRemark(remark);
        return outDto;
    }

    @Override
    public boolean ymk01160_0010(YMK01160Service0010InDto inDto) {
        SysUserCard sysUserCard = inDto.getSysUserCard();
        SysUserStatus sysUserStatus = mongoTemplate.findOne(new Query(Criteria.where("userId").is(SecurityUtils.getUserId())),
                SysUserStatus.class, MongoConstant.MONGODB_NAME_USERSTATUS);
        sysUserCard.setCardType("6");
        if (sysUserStatus.getIsCompany().equals("0")) {
            sysUserCard.setId(mongoDBUtil.getId(SysUserCard.class, MongoConstant.MONGODB_NAME_USERCARD));
        }
        sysUserCard.setStatus("1");
        mongoTemplate.save(sysUserCard, MongoConstant.MONGODB_NAME_USERCARD);
        sysUserStatus.setIsCompany("1");
        mongoTemplate.save(sysUserStatus, MongoConstant.MONGODB_NAME_USERSTATUS);
        return true;
    }
}
