package com.ruoyi.service.service;

import com.ruoyi.common.mongValue.SysUserCard;
import com.ruoyi.service.Dto.YMK01100Service0000OutDto;
import com.ruoyi.service.Dto.YMK01140Service0000OutDto;
import com.ruoyi.service.Dto.YMK01140Service0030InDto;

public interface YMK01100Service {
    YMK01100Service0000OutDto ymk01100_0000();
}
