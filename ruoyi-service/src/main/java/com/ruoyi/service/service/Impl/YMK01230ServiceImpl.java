package com.ruoyi.service.service.Impl;

import com.ruoyi.common.constant.MongoConstant;
import com.ruoyi.common.core.Service.BaseServiceImpl;
import com.ruoyi.common.mongValue.CraneMongoValue;
import com.ruoyi.common.mongValue.SveCranes;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.service.Dto.YMK01230Service0030InDto;
import com.ruoyi.service.Dto.YMK01230Service0040InDto;
import com.ruoyi.service.service.YMK01230Service;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * 起重机类型
 *
 * @author ruoyi
 * @date 2024-03-29
 */
@Service
public class YMK01230ServiceImpl  extends BaseServiceImpl implements YMK01230Service {

    /**
     * 初期表示
     * @return
     */
    @Override
    public List<CraneMongoValue> ymk01230_0000(CraneMongoValue craneMongoValue) {
        Query query = new Query();
        if (!StringUtils.isEmpty(craneMongoValue.getCraneType())) {
            query.addCriteria(Criteria.where("craneType").is(craneMongoValue.getCraneType()));
        }
        if (!StringUtils.isEmpty(craneMongoValue.getPowerSource())) {
            query.addCriteria(Criteria.where("powerSource").is(craneMongoValue.getPowerSource()));
        }
        return mongoTemplate.find(query, CraneMongoValue.class,MongoConstant.MONGODB_NAME_CRANETYPE);
    }

    /**
     * 获取起重机信息详细信息
     * @param craneId
     * @return
     */
    @Override
    public CraneMongoValue ymk01230_0010(Long craneId) {
        Criteria criteria = Criteria.where("_id").is(craneId);
        Query query = new Query(criteria);
        CraneMongoValue craneMongoValue = mongoTemplate.findOne(query, CraneMongoValue.class,MongoConstant.MONGODB_NAME_CRANETYPE);
        return craneMongoValue;
    }

    /**
     * 添加起重机类型
     * @param sveCranes
     * @return
     */
    @Override
    public boolean ymk01230_0020(CraneMongoValue sveCranes) {
        sveCranes.setId(mongoDBUtil.getId(CraneMongoValue.class, MongoConstant.MONGODB_NAME_CRANETYPE));
        mongoTemplate.save(sveCranes, MongoConstant.MONGODB_NAME_CRANETYPE);
        return true;
    }

    /**
     * 修改起重机
     * @param inDto
     * @return
     */
    @Override
    public boolean ymk01230_0030(YMK01230Service0030InDto inDto) {
        mongoTemplate.save(inDto.getCraneMongoValue(), MongoConstant.MONGODB_NAME_CRANETYPE);
        return true;
    }

    /**
     * 删除起重机
     * @param inDto
     * @return
     */
    @Override
    public boolean ymk01230_0040(YMK01230Service0040InDto inDto) {
        Criteria criteria = Criteria.where("_id").is(inDto.getId());
        Query query = new Query(criteria);
        CraneMongoValue craneMongoValue = mongoTemplate.findOne(query, CraneMongoValue.class,MongoConstant.MONGODB_NAME_CRANETYPE);
        craneMongoValue.setDelFlag("1");
        mongoTemplate.save(craneMongoValue, MongoConstant.MONGODB_NAME_CRANETYPE);
        return true;
    }

}
