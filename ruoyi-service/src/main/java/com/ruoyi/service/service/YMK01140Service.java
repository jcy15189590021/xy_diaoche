package com.ruoyi.service.service;

import com.ruoyi.common.mongValue.SysUserCard;
import com.ruoyi.service.Dto.YMK01140Service0000OutDto;
import com.ruoyi.service.Dto.YMK01140Service0030InDto;

public interface YMK01140Service {
    YMK01140Service0000OutDto ymk01140_0000(SysUserCard sysUserCard);

    SysUserCard ymk01140_0010(Long userId);

    int ymk01140_0020(SysUserCard sysUserCard);

    int ymk01140_0030(YMK01140Service0030InDto inDto);
}
