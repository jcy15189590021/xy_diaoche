package com.ruoyi.service.service;

import com.ruoyi.service.Dto.*;
import com.ruoyi.common.mongValue.SveCranes;

import java.util.List;

public interface YMK01240Service {

    List<SveCranes> ymk01240_0000(YMK01240Service0000InDto inDto);

    int ymk01240_0010(YMK01240Service0010InDto inDto);

    int ymk01240_0020(YMK01240Service0020InDto inDto);

    YMK01240Service0030OutDto ymk01240_0030(Long craneId);

    YMK01240Service0040OutDto ymk01240_0040();

    int ymk01240_0050(YMK01240Service0050InDto indto);

    boolean ymk01240_0060(Long craneId);
}
