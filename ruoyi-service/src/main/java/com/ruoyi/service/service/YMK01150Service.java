package com.ruoyi.service.service;

import com.ruoyi.service.Dto.YMK01150Service0000OutDto;
import com.ruoyi.service.Dto.YMK01150Service0010InDto;

public interface YMK01150Service {

    YMK01150Service0000OutDto ymk01150_0000();

    boolean ymk01150_0010(YMK01150Service0010InDto inDto);
}
