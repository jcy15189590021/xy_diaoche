package com.ruoyi.service.service.Impl;

import cn.hutool.core.bean.BeanUtil;
import com.ruoyi.common.core.Service.BaseServiceImpl;
import com.ruoyi.common.core.domain.entity.SysDept;
import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.core.domain.model.LoginUser;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.service.Dto.YMK01280Service0000OutDto;
import com.ruoyi.service.Dto.YMK01280Service0010InDto;
import com.ruoyi.service.Dto.YMK01280Service0010OutDto;
import com.ruoyi.service.Dto.YMK01280Service0020InDto;
import com.ruoyi.common.constant.MongoConstant;
import com.ruoyi.common.mongValue.SveCranes;
import com.ruoyi.service.service.YMK01280Service;
import com.ruoyi.common.mongValue.OrderAddValue;
import com.ruoyi.system.service.ISysDeptService;
import com.ruoyi.system.service.ISysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;


@Service
public class YMK01280ServiceImpl extends BaseServiceImpl implements YMK01280Service{

    @Autowired
    private ISysUserService sysUserService;

    @Autowired
    private ISysDeptService sysDeptService;

    /**
     * 初期化
     * @return
     */
    @Override
    public YMK01280Service0000OutDto ymk01280_0000(SveCranes sveCrane) {
        LoginUser user = SecurityUtils.getLoginUser();
        Query query = new Query();
        if (!SecurityUtils.isAdmin(user.getUserId())) {
            query.addCriteria(new Criteria().where("companyId").ne(user.getCompanyId()).orOperator(Criteria.where("userId").ne(user.getUserId())));
        }
        query.addCriteria(new Criteria().and("isQualify").is("2").and("isLease").is("1"));
        List<SveCranes> sveCranes = mongoTemplate.find(query,SveCranes.class, MongoConstant.MONGODB_NAME_SVECRANE);

        YMK01280Service0000OutDto outDto = new YMK01280Service0000OutDto();
        outDto.setSveCranes(sveCranes.stream()
                .filter(sveCranef -> sveCrane.getName() != null ? sveCranef.getName().equals(sveCrane.getName()) : true)
                .filter(sveCranef -> sveCrane.getCraneType() != null ? sveCranef.getCraneType().equals(sveCrane.getCraneType()) : true)
                .filter(sveCranef -> sveCrane.getCapacityMin() != null ? sveCranef.getCapacityMin().compareTo(sveCrane.getCapacityMin()) >= 0 : true)
                .filter(sveCranef -> sveCrane.getCapacityMax() != null ? sveCranef.getCapacityMax().compareTo(sveCrane.getCapacityMax()) <= 0 : true)
                .filter(sveCranef -> sveCrane.getLiftingSpeed() != null ? sveCranef.getLiftingSpeed().compareTo(sveCrane.getLiftingSpeed()) >= 0 : true)
                .filter(sveCranef -> sveCrane.getTravelSpeed() != null ? sveCranef.getTravelSpeed().compareTo(sveCrane.getTravelSpeed()) >= 0 : true)
                .filter(sveCranef -> sveCrane.getMaxLiftingHeight() != null ? sveCranef.getMaxLiftingHeight().compareTo(sveCrane.getMaxLiftingHeight()) <= 0 : true)
                .filter(sveCranef -> sveCrane.getMaxRadius() != null ? Long.valueOf(sveCranef.getMaxRadius()) <= Long.valueOf(sveCrane.getMaxRadius()) : true)
                .filter(sveCranef -> sveCrane.getPowerSource() != null ? sveCranef.getPowerSource().equals(sveCrane.getPowerSource()) : true)
                .filter(sveCranef -> sveCrane.getIsLease() != null ? sveCranef.getIsLease().equals(sveCrane.getIsLease()) : true)
                .filter(sveCranef -> sveCrane.getIsQualify() != null ? sveCranef.getIsQualify().equals(sveCrane.getIsQualify()) : true)
                .filter(sveCranef -> sveCranef.getDelFlag().equals("0"))
                .collect(Collectors.toList()));
        return outDto;
    }

    /**
     * 租赁前信息查询
     * @param inDto
     * @return
     */
    @Override
    public YMK01280Service0010OutDto ymk01280_0010(YMK01280Service0010InDto inDto) {
        //租赁的起重机信息
        SveCranes sveCrane = mongoTemplate.findOne(new Query(Criteria.where("id").is(inDto.getId())),
                SveCranes.class,MongoConstant.MONGODB_NAME_SVECRANE);
        //甲方信息：当前登录的User信息
        LoginUser user = SecurityUtils.getLoginUser();
        //乙方信息
        SysUser partyBUser = new SysUser();
        if (sveCrane.getUserId() != null) {
            partyBUser = sysUserService.selectUserById(sveCrane.getUserId());
        }
        SysDept sysDept = new SysDept();
        if (sveCrane.getCompanyId() != null) {
            sysDept = sysDeptService.selectDeptById(sveCrane.getCompanyId());
        } else if (partyBUser.getCompanyId() != null) {
            sysDept = sysDeptService.selectDeptById(partyBUser.getCompanyId());
        }

        OrderAddValue orderAddValue = new OrderAddValue();
        orderAddValue.setPartyAUName(user.getUser().getNickName());
        orderAddValue.setPartyAUid(user.getUserId());
        if (!BeanUtil.isEmpty(sysDept)) {
            orderAddValue.setPartyBCName(sysDept.getDeptName());
        }
        orderAddValue.setPartyBCid(partyBUser.getUserId());
        orderAddValue.setPartyBUName(partyBUser.getNickName());
        orderAddValue.setCraneId(sveCrane.getId());
        orderAddValue.setCraneName(sveCrane.getName());
        orderAddValue.setRentalRate(new BigDecimal(sveCrane.getMoney()));
        orderAddValue.setPartyBUid(sveCrane.getUserId());

        YMK01280Service0010OutDto outDto = new YMK01280Service0010OutDto();
        outDto.setOrderAddValue(orderAddValue);
        return outDto;
    }

    /**
     * 添加订单
     * @param inDto
     * @return
     */
    @Override
    public int ymk01280_0020(YMK01280Service0020InDto inDto) {

        OrderAddValue orderAddValue = inDto.getOrderAddValue();
        orderAddValue.setId(mongoDBUtil.getId(OrderAddValue.class,MongoConstant.MONGODB_NAME_ORDERADD));
        orderAddValue.setTotalDays(DateUtils.differentDaysByMillisecond(orderAddValue.getRentalEndDate(),orderAddValue.getRentalStartDate()));
        orderAddValue.setTotalAmount(
                orderAddValue.getRentalRate().multiply(new BigDecimal(orderAddValue.getTotalDays()))
        );
        orderAddValue.setBillStatus("0");
        mongoTemplate.save(orderAddValue,MongoConstant.MONGODB_NAME_ORDERADD);
        SveCranes sveCrane = mongoTemplate.findOne(new Query(Criteria.where("id").is(orderAddValue.getCraneId())),
                SveCranes.class,MongoConstant.MONGODB_NAME_SVECRANE);
        sveCrane.setIsLease("0");
        mongoTemplate.save(sveCrane,MongoConstant.MONGODB_NAME_SVECRANE);
        return 1;
    }

}
