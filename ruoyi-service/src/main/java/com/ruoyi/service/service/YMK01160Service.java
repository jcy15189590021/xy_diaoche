package com.ruoyi.service.service;

import com.ruoyi.service.Dto.YMK01160Service0000OutDto;
import com.ruoyi.service.Dto.YMK01160Service0010InDto;

public interface YMK01160Service {
    YMK01160Service0000OutDto ymk01160_0000();

    boolean ymk01160_0010(YMK01160Service0010InDto inDto);
}
