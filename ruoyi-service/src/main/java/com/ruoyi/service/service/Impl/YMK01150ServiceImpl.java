package com.ruoyi.service.service.Impl;

import com.ruoyi.common.constant.MongoConstant;
import com.ruoyi.common.core.Service.BaseServiceImpl;
import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.mongValue.QualifyLog;
import com.ruoyi.common.mongValue.SysUserStatus;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.service.Dto.YMK01150Service0000OutDto;
import com.ruoyi.common.mongValue.SysUserCard;
import com.ruoyi.service.Dto.YMK01150Service0010InDto;
import com.ruoyi.service.service.YMK01150Service;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

@Service
public class YMK01150ServiceImpl extends BaseServiceImpl implements YMK01150Service {

    /**
     * 资格认证初期化
     * @return
     */
    @Override
    public YMK01150Service0000OutDto ymk01150_0000() {
        YMK01150Service0000OutDto outDto = new YMK01150Service0000OutDto();
        SysUser user = SecurityUtils.getLoginUser().getUser();
        logger.info(user.toString());
        SysUserStatus sysUserStatus = new SysUserStatus();
        Query query = new Query();
        query.addCriteria(Criteria.where("userId").is(user.getUserId()));
        sysUserStatus = mongoTemplate.findOne(query, SysUserStatus.class, MongoConstant.MONGODB_NAME_USERSTATUS);
        String remark = StringUtils.EMPTY;
        SysUserCard sysUserCard = new SysUserCard();
        sysUserCard.setCardType("1");
        if (sysUserStatus.getIsReal().equals("0")) {
            outDto.setSysUserCard(sysUserCard);
        } else {
            Query query2 = new Query();
            query2.addCriteria(Criteria.where("userId").is(user.getUserId()));
            query2.addCriteria(Criteria.where("cardType").is("1"));
            sysUserCard = mongoTemplate.findOne(query2, SysUserCard.class, MongoConstant.MONGODB_NAME_USERCARD);
            outDto.setSysUserCard(sysUserCard);

            if (sysUserStatus.getIsReal().equals("3")) {
                QualifyLog qualifyLog = mongoTemplate.findOne(new Query(Criteria.where("id").is(sysUserCard.getQualifyLogId())),
                        QualifyLog.class, MongoConstant.MONGODB_NAME_QUALIFYLOG);
                remark = qualifyLog.getRemark();
            }
        }
        outDto.setIsReal(sysUserStatus.getIsReal());
        outDto.setRemark(remark);
        return outDto;
    }

    /**
     * 资格认证更新
     * @param inDto
     * @return
     */
    @Override
    public boolean ymk01150_0010(YMK01150Service0010InDto inDto) {
        SysUserCard sysUserCard = inDto.getSysUserCard();
        SysUserStatus sysUserStatus = mongoTemplate.findOne(new Query(Criteria.where("userId").is(SecurityUtils.getUserId())),
                SysUserStatus.class, MongoConstant.MONGODB_NAME_USERSTATUS);
        sysUserCard.setCardType("1");
        if (sysUserStatus.getIsReal().equals("0")) {
            sysUserCard.setId(mongoDBUtil.getId(SysUserCard.class, MongoConstant.MONGODB_NAME_USERCARD));
        }
        sysUserCard.setStatus("1");
        mongoTemplate.save(sysUserCard, MongoConstant.MONGODB_NAME_USERCARD);
        sysUserStatus.setIsReal("1");
        mongoTemplate.save(sysUserStatus, MongoConstant.MONGODB_NAME_USERSTATUS);
        return true;
    }
}
