package com.ruoyi.service.service.Impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.ruoyi.common.config.RuoYiConfig;
import com.ruoyi.common.constant.MongoConstant;
import com.ruoyi.common.core.Service.BaseServiceImpl;
import com.ruoyi.common.core.domain.entity.SysDept;
import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.mongValue.QualifyLog;
import com.ruoyi.common.mongValue.SysUserCard;
import com.ruoyi.common.mongValue.SysUserStatus;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.framework.web.domain.server.Sys;
import com.ruoyi.service.Dto.YMK01140Service0000OutDto;
import com.ruoyi.service.Dto.YMK01140Service0030InDto;
import com.ruoyi.service.service.YMK01140Service;
import com.ruoyi.system.domain.SysUserRole;
import com.ruoyi.system.mapper.SysDeptMapper;
import com.ruoyi.system.service.ISysDeptService;
import com.ruoyi.system.service.ISysRoleService;
import com.ruoyi.system.service.ISysUserService;
import com.ruoyi.system.service.SysUserRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 资格认证业务
 */
@Service
public class YMK01140ServiceImpl extends BaseServiceImpl implements YMK01140Service {

    @Autowired
    private ISysUserService sysUserService;

    @Autowired
    private ISysDeptService sysDeptService;

    @Autowired
    private SysDeptMapper deptMapper;

    @Autowired
    private SysUserRoleService sysUserRoleService;

    @Autowired
    private RuoYiConfig ruoYiConfig;

    /**
     * 初期化
     * @return
     */
    @Override
    public YMK01140Service0000OutDto ymk01140_0000(SysUserCard sysUserCard) {
        YMK01140Service0000OutDto outDto = new YMK01140Service0000OutDto();
        List<SysUserCard> sysUserCards = mongoTemplate.find(new Query(), SysUserCard.class, MongoConstant.MONGODB_NAME_USERCARD);
        outDto.setSysUserCards(sysUserCards.stream()
                .filter(userCard -> sysUserCard.getCardType() != null ? userCard.getCardType().equals(sysUserCard.getCardType()) : true)
                .filter(userCard -> sysUserCard.getStatus() != null ? userCard.getStatus().equals(sysUserCard.getStatus()) : true)
                .collect(Collectors.toList()));
        return outDto;
    }

    /**
     * 审核前查询
     * @param userId
     * @return
     */
    @Override
    public SysUserCard ymk01140_0010(Long userId) {
        SysUserCard sysUserCard = mongoTemplate.findOne(new Query(Criteria.where("userId").is(userId)),
                SysUserCard.class, MongoConstant.MONGODB_NAME_USERCARD);

        return sysUserCard;
    }

    /**
     * 审核认证通过
     * @param sysUserCard
     * @return
     */
    @Override
    public int ymk01140_0020(SysUserCard sysUserCard) {
        sysUserCard.setStatus("2");
        QualifyLog qualifyLog = new QualifyLog();
        qualifyLog.setId(mongoDBUtil.getId(QualifyLog.class, MongoConstant.MONGODB_NAME_QUALIFYLOG));
        qualifyLog.setIsQualify("2");
        qualifyLog.setRemark("已通过");
        qualifyLog.setQualifyType(sysUserCard.getCardType());
        qualifyLog.setCreateTime(DateUtils.getNowDate());
        // 更新审核记录
        mongoTemplate.save(qualifyLog, MongoConstant.MONGODB_NAME_QUALIFYLOG);

        SysUserStatus sysUserStatus = mongoTemplate.findOne(new Query(Criteria.where("userId").is(sysUserCard.getUserId())),
                SysUserStatus.class, MongoConstant.MONGODB_NAME_USERSTATUS);
        if (sysUserCard.getCardType().equals("1")) {
            sysUserStatus.setIsReal("2");
        } else if (sysUserCard.getCardType().equals("6")) {
            sysUserStatus.setIsCompany("2");
        }
        // 更新用户状态
        mongoTemplate.save(sysUserStatus,MongoConstant.MONGODB_NAME_USERSTATUS);
        sysUserCard.setQualifyLogId(qualifyLog.getId());
        mongoTemplate.save(sysUserCard, MongoConstant.MONGODB_NAME_USERCARD);

        boolean flag = false;
        if (sysUserCard.getCardType().equals("1")) {
            flag = sysUserService.update(new UpdateWrapper<SysUser>().eq("user_id", sysUserCard.getUserId())
                    .set("nick_name", sysUserCard.getRealName()));
            // 添加角色
            SysUserRole userRole = new SysUserRole();
            userRole.setUserId(sysUserCard.getUserId());
            userRole.setRoleId(Long.valueOf(ruoYiConfig.getCustomerroleId()));
            sysUserRoleService.remove(new QueryWrapper<SysUserRole>().eq("user_id", sysUserCard.getUserId()));
            sysUserRoleService.save(userRole);
        } else if (sysUserCard.getCardType().equals("6")) {
            //更新公司信息
            this.updateInfo(sysUserCard);
            flag = true;
        }

        return flag ? 1:0;
    }

    /**
     * 审核不通过
     * @param inDto
     * @return
     */
    @Override
    public int ymk01140_0030(YMK01140Service0030InDto inDto) {
        SysUserCard sysUserCard = inDto.getSysUserCard();
        sysUserCard.setStatus("3");
        QualifyLog qualifyLog = new QualifyLog();
        qualifyLog.setId(mongoDBUtil.getId(QualifyLog.class, MongoConstant.MONGODB_NAME_QUALIFYLOG));
        qualifyLog.setIsQualify("3");
        qualifyLog.setRemark(inDto.getRemark());
        qualifyLog.setQualifyType(sysUserCard.getCardType());
        qualifyLog.setCreateTime(DateUtils.getNowDate());
        // 更新审核记录
        mongoTemplate.save(qualifyLog, MongoConstant.MONGODB_NAME_QUALIFYLOG);

        SysUserStatus sysUserStatus = mongoTemplate.findOne(new Query(Criteria.where("userId").is(sysUserCard.getUserId())),
                SysUserStatus.class, MongoConstant.MONGODB_NAME_USERSTATUS);
        if (sysUserCard.getCardType().equals("1")) {
            sysUserStatus.setIsReal("3");
        } else if (sysUserCard.getCardType().equals("6")) {
            sysUserStatus.setIsCompany("3");
        }
        // 更新用户状态
        mongoTemplate.save(sysUserStatus,MongoConstant.MONGODB_NAME_USERSTATUS);
        sysUserCard.setQualifyLogId(qualifyLog.getId());
        mongoTemplate.save(sysUserCard, MongoConstant.MONGODB_NAME_USERCARD);

        return 1;
    }

    private void updateInfo(SysUserCard sysUserCard) {
        SysUser sysUser = sysUserService.getOne(new QueryWrapper<SysUser>().eq("user_id",sysUserCard.getUserId()));
        List<SysDept> depts = sysDeptService.list(new QueryWrapper<SysDept>().eq("ancestors","0,100"));
        // 添加公司
        SysDept parentDept = new SysDept();
        parentDept.setDeptId(deptMapper.getLastId() + 1);
        parentDept.setParentId(100L);
        parentDept.setAncestors("0,100");
        parentDept.setDeptName(sysUserCard.getRealName());
        parentDept.setOrderNum(depts.size() + 1);
        parentDept.setLeader(sysUser.getNickName());
        parentDept.setPhone(sysUser.getPhonenumber());
        parentDept.setEmail(sysUser.getEmail());
        parentDept.setDelFlag("0");
        parentDept.setCreateBy(SecurityUtils.getUsername());
        parentDept.setCreateTime(DateUtils.getNowDate());
        parentDept.setUpdateBy(SecurityUtils.getUsername());
        parentDept.setUpdateTime(DateUtils.getNowDate());
        sysDeptService.save(parentDept);
        // 添加部门
        SysDept childDept = new SysDept();
        childDept.setDeptId(deptMapper.getLastId() + 1);
        childDept.setParentId(parentDept.getDeptId());
        childDept.setAncestors("0,100," + String.valueOf(parentDept.getDeptId()));
        childDept.setDeptName("业务部门");
        childDept.setOrderNum(1);
        childDept.setLeader(sysUser.getNickName());
        childDept.setPhone(sysUser.getPhonenumber());
        childDept.setEmail(sysUser.getEmail());
        childDept.setDelFlag("0");
        childDept.setCreateBy(SecurityUtils.getUsername());
        childDept.setCreateTime(DateUtils.getNowDate());
        childDept.setUpdateBy(SecurityUtils.getUsername());
        childDept.setUpdateTime(DateUtils.getNowDate());
        sysDeptService.save(childDept);

        // 添加角色
        SysUserRole userRole = new SysUserRole();
        userRole.setUserId(sysUser.getUserId());
        userRole.setRoleId(Long.valueOf(ruoYiConfig.getCompanyadminroleId()));
        sysUserRoleService.remove(new QueryWrapper<SysUserRole>().eq("user_id", sysUser.getUserId()));
        sysUserRoleService.save(userRole);

        // 更新用户信息
        sysUser.setCompanyId(parentDept.getDeptId());
        sysUserService.update(new UpdateWrapper<SysUser>()
                .eq("user_id",sysUser.getUserId())
                .set("company_id", parentDept.getDeptId())
                .set("dept_id",parentDept.getDeptId()));
    }
}
