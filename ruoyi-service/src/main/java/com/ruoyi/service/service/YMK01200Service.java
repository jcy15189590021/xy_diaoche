package com.ruoyi.service.service;

import com.ruoyi.service.Dto.*;

public interface YMK01200Service {

    YMK01200Service0000OutDto ymk01200_0000();

    YMK01200Service0010OutDto ymk01280_0010(YMK01200Service0010InDto inDto);

    boolean ymk01200_0020(YMK01200Service0020InDto inDto);

    boolean ymk01200_0030(YMK01200Service0030InDto inDto);

    boolean ymk01200_0040(YMK01200Service0040InDto inDto);

    boolean ymk01200_0050(YMK01200Service0050InDto inDto);

    boolean ymk01200_0060(YMK01200Service0060InDto inDto);

    boolean ymk01200_0070(YMK01200Service0070InDto inDto);

    boolean ymk01200_0080(YMK01200Service0080InDto inDto);
}
