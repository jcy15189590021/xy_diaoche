package com.ruoyi.service.service.Impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.ruoyi.common.constant.MongoConstant;
import com.ruoyi.common.core.Service.BaseServiceImpl;
import com.ruoyi.common.core.domain.entity.SysDept;
import com.ruoyi.common.core.domain.entity.SysDictData;
import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.mongValue.CraneMongoValue;
import com.ruoyi.common.mongValue.OrderAddValue;
import com.ruoyi.common.mongValue.SveCranes;
import com.ruoyi.common.mongValue.SysUserStatus;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.DictUtils;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.common.value.ChartValue;
import com.ruoyi.common.value.PanelGroup;
import com.ruoyi.service.Dto.YMK01100Service0000OutDto;
import com.ruoyi.service.service.YMK01100Service;
import com.ruoyi.system.service.ISysDeptService;
import com.ruoyi.system.service.ISysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;


/**
 * 资格认证业务
 */
@Service
public class YMK01100ServiceImpl extends BaseServiceImpl implements YMK01100Service {

    @Autowired
    private ISysUserService sysUserService;

    @Autowired
    private ISysDeptService sysDeptService;

    @Override
    public YMK01100Service0000OutDto ymk01100_0000() {
        YMK01100Service0000OutDto outDto = new YMK01100Service0000OutDto();
        // 上部计数
        PanelGroup panelGroup = new PanelGroup();
        // 在线人数
        panelGroup.setUserNum(sysUserService.count(new QueryWrapper<SysUser>().ne("del_flag","2")));
        // 在线商家
        panelGroup.setShopNum(sysDeptService.count(new QueryWrapper<SysDept>().ne("parent_id",100L)));
        // 起重机类型
        panelGroup.setCraneTypeNum(mongoTemplate.count(new Query(), MongoConstant.MONGODB_NAME_CRANETYPE));
        // 起重机数量
        panelGroup.setCraneNum(mongoTemplate.count(new Query(), MongoConstant.MONGODB_NAME_SVECRANE));
        outDto.setPanelGroup(panelGroup);

        // 中部折线图
        // 成交额折线图
        // 顾客/管理员：平台日总成交额
        // 商家：各自日成交额
        ChartValue billChart = getBillChart();
        ChartValue billcountChart = new ChartValue();
        billcountChart.setCData(billChart.getChartDatas().get(0));
        billcountChart.setXData(billChart.getXData());
        billcountChart.setChartname("日成交量(单)");
        ChartValue billAmountChart = new ChartValue();
        billAmountChart.setCData(billChart.getChartDatas().get(1));
        billAmountChart.setXData(billChart.getXData());
        billAmountChart.setChartname("日成交额(元)");
        outDto.setBillCountChart(billcountChart);
        outDto.setBillAmountChart(billAmountChart);

        // 中部柱状图
        // 起重机类型柱状图
        outDto.setCraneChart(getCraneType());
        // 底部订单信息


        return outDto;
    }

    /**
     * 折线图数据做成
     * @return
     */
    private ChartValue getBillChart() {
        ChartValue billChart = new ChartValue();
        SysUser user = SecurityUtils.getLoginUser().getUser();
        Query query = new Query();
        if (!SecurityUtils.isAdmin(user.getUserId())) {
            SysUserStatus sysUserStatus = mongoTemplate.findOne(new Query().addCriteria(Criteria.where("userId").is(user.getUserId())),
                    SysUserStatus.class, MongoConstant.MONGODB_NAME_USERSTATUS);
            if (sysUserStatus.getIdentity().equals("1")) {
                query.addCriteria(Criteria.where("partyBCid").is(user.getCompanyId()));
            }
        }
        List<OrderAddValue> orders = mongoTemplate.find(query, OrderAddValue.class ,MongoConstant.MONGODB_NAME_ORDERADD);
        int[] chartData1 = new int[7];
        int[] chartData2 = new int[7];
        String[] xData = new String[7];
        xData[0] = DateUtils.dateTime(DateUtils.minusDays(DateUtils.getNowDate(), 7));
        xData[1] = DateUtils.dateTime(DateUtils.minusDays(DateUtils.getNowDate(), 6));
        xData[2] = DateUtils.dateTime(DateUtils.minusDays(DateUtils.getNowDate(), 5));
        xData[3] = DateUtils.dateTime(DateUtils.minusDays(DateUtils.getNowDate(), 4));
        xData[4] = DateUtils.dateTime(DateUtils.minusDays(DateUtils.getNowDate(), 3));
        xData[5] = DateUtils.dateTime(DateUtils.minusDays(DateUtils.getNowDate(), 2));
        xData[6] = DateUtils.dateTime(DateUtils.minusDays(DateUtils.getNowDate(), 1));

        // chartData
        int datasum1 = 0;int datasum2 = 0;int datasum3 = 0;int datasum4 = 0;
        int datasum5 = 0;int datasum6 = 0;int datasum7 = 0;

        BigDecimal totalAmount1 = BigDecimal.ZERO;BigDecimal totalAmount2 = BigDecimal.ZERO;BigDecimal totalAmount3 = BigDecimal.ZERO;
        BigDecimal totalAmount4 = BigDecimal.ZERO;BigDecimal totalAmount5 = BigDecimal.ZERO;BigDecimal totalAmount6 = BigDecimal.ZERO;
        BigDecimal totalAmount7 = BigDecimal.ZERO;

        for (OrderAddValue order: orders) {
            if (DateUtils.dateTime(order.getRentalEndDate()).equals(xData[0])) {
                datasum1++;
                totalAmount1 = totalAmount1.add(order.getTotalAmount());
            } else if (DateUtils.dateTime(order.getRentalEndDate()).equals(xData[1])) {
                datasum2++;
                totalAmount2 = totalAmount2.add(order.getTotalAmount());
            } else if (DateUtils.dateTime(order.getRentalEndDate()).equals(xData[2])) {
                datasum3++;
                totalAmount3 = totalAmount3.add(order.getTotalAmount());
            } else if (DateUtils.dateTime(order.getRentalEndDate()).equals(xData[3])) {
                datasum4++;
                totalAmount4 = totalAmount4.add(order.getTotalAmount());
            } else if (DateUtils.dateTime(order.getRentalEndDate()).equals(xData[4])) {
                datasum5++;
                totalAmount5 = totalAmount5.add(order.getTotalAmount());
            } else if (DateUtils.dateTime(order.getRentalEndDate()).equals(xData[5])) {
                datasum6++;
                totalAmount6 = totalAmount6.add(order.getTotalAmount());
            } else if (DateUtils.dateTime(order.getRentalEndDate()).equals(xData[6])) {
                datasum7++;
                totalAmount7 = totalAmount7.add(order.getTotalAmount());
            }
        }

        chartData1[0] = datasum1;
        chartData2[0] = totalAmount1.intValue();
        chartData1[1] = datasum2;
        chartData2[1] = totalAmount2.intValue();
        chartData1[2] = datasum3;
        chartData2[2] = totalAmount3.intValue();
        chartData1[3] = datasum4;
        chartData2[3] = totalAmount4.intValue();
        chartData1[4] = datasum5;
        chartData2[4] = totalAmount5.intValue();
        chartData1[5] = datasum6;
        chartData2[5] = totalAmount6.intValue();
        chartData1[6] = datasum7;
        chartData2[6] = totalAmount7.intValue();

        List<int[]> chartDatas = new ArrayList<>();
        chartDatas.add(chartData1);
        chartDatas.add(chartData2);

        billChart.setXData(xData);
        billChart.setChartDatas(chartDatas);
        return billChart;
    }

    private ChartValue getCraneType() {
        ChartValue chartValue = new ChartValue();
        List<SveCranes> sveCranes = mongoTemplate.find(new Query(), SveCranes.class, MongoConstant.MONGODB_NAME_SVECRANE);
        List<SysDictData> sysDictData = DictUtils.getDictCache("service_crane_type");

        int[] chartData = new int[sysDictData.size()];
        String[] xData = new String[sysDictData.size()];
        for (int i = 0; i < sysDictData.size(); i++) {
            xData[i] = sysDictData.get(i).getDictLabel();
        }

        for (SveCranes sveCrane : sveCranes) {
            int tmp = chartData[Integer.parseInt(sveCrane.getCraneType()) - 1];
            chartData[Integer.parseInt(sveCrane.getCraneType()) - 1] = tmp == 0 ? 1 : tmp+1;
        }

        chartValue.setCData(chartData);
        chartValue.setXData(xData);
        chartValue.setChartname("起重机类型(种)");

        return chartValue;
    }


}
