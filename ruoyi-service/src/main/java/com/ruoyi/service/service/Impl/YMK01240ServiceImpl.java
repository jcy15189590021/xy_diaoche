package com.ruoyi.service.service.Impl;

import com.ruoyi.common.core.Service.BaseServiceImpl;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.service.Dto.*;
import com.ruoyi.common.constant.MongoConstant;
import com.ruoyi.common.mongValue.SveCranes;
import com.ruoyi.common.mongValue.CraneMongoValue;
import com.ruoyi.common.mongValue.QualifyLog;
import com.ruoyi.service.service.YMK01240Service;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class YMK01240ServiceImpl extends BaseServiceImpl implements YMK01240Service {

    /**
     * 初期表示
     * @param inDto
     * @return
     */
    @Override
    public List<SveCranes> ymk01240_0000(YMK01240Service0000InDto inDto) {
        SveCranes sveCrane = inDto.getSveCrane();
        Criteria criteria = new Criteria();
        if (!SecurityUtils.isAdmin(SecurityUtils.getLoginUser().getUserId()) || SecurityUtils.getLoginUser().getCompanyId() != 0) {
            criteria.where("userId").is(SecurityUtils.getUserId())
                    .orOperator(criteria.where("companyId").is(SecurityUtils.getLoginUser().getCompanyId()));
        }

        Query query = new Query(criteria);
        List<SveCranes> sveCranes = mongoTemplate.find(query,SveCranes.class,MongoConstant.MONGODB_NAME_SVECRANE);

        return sveCranes.stream()
                .filter(sveCranef -> sveCrane.getName() != null ? sveCranef.getName().equals(sveCrane.getName()) : true)
                .filter(sveCranef -> sveCrane.getCraneType() != null ? sveCranef.getCraneType().equals(sveCrane.getCraneType()) : true)
                .filter(sveCranef -> sveCrane.getCapacityMin() != null ? sveCranef.getCapacityMin().compareTo(sveCrane.getCapacityMin()) >= 0 : true)
                .filter(sveCranef -> sveCrane.getCapacityMax() != null ? sveCranef.getCapacityMax().compareTo(sveCrane.getCapacityMax()) <= 0 : true)
                .filter(sveCranef -> sveCrane.getLiftingSpeed() != null ? sveCranef.getLiftingSpeed().compareTo(sveCrane.getLiftingSpeed()) >= 0 : true)
                .filter(sveCranef -> sveCrane.getTravelSpeed() != null ? sveCranef.getTravelSpeed().compareTo(sveCrane.getTravelSpeed()) >= 0 : true)
                .filter(sveCranef -> sveCrane.getMaxLiftingHeight() != null ? sveCranef.getMaxLiftingHeight().compareTo(sveCrane.getMaxLiftingHeight()) <= 0 : true)
                .filter(sveCranef -> sveCrane.getMaxRadius() != null ? Long.valueOf(sveCranef.getMaxRadius()) <= Long.valueOf(sveCrane.getMaxRadius()) : true)
                .filter(sveCranef -> sveCrane.getPowerSource() != null ? sveCranef.getPowerSource().equals(sveCrane.getPowerSource()) : true)
                .filter(sveCranef -> sveCrane.getIsLease() != null ? sveCranef.getIsLease().equals(sveCrane.getIsLease()) : true)
                .filter(sveCranef -> sveCrane.getIsQualify() != null ? sveCranef.getIsQualify().equals(sveCrane.getIsQualify()) : true)
                .filter(sveCranef -> sveCrane.getDelFlag() != null ? sveCranef.getDelFlag().equals(sveCrane.getDelFlag()) : true)
                .collect(Collectors.toList());
    }

    /**
     * 用户更新起重机
     * @param inDto
     * @return
     */
    @Override
    public int ymk01240_0010(YMK01240Service0010InDto inDto) {
        SveCranes sveCrane = inDto.getSveCrane();
        sveCrane.setUserId(SecurityUtils.getUserId());
        if (SecurityUtils.getLoginUser().getCompanyId() != null) {
            sveCrane.setCompanyId(SecurityUtils.getLoginUser().getCompanyId());
        }
        if (sveCrane.getIsQualify().equals("3")) {
            sveCrane.setIsQualify("1");
        }
        try {
            mongoTemplate.save(sveCrane,MongoConstant.MONGODB_NAME_SVECRANE);
        }catch (Exception e) {
            logger.error(e.getMessage());
        }
        return 1;
    }

    /**
     * 审核起重机
     * @param inDto
     * @return
     */
    @Override
    public int ymk01240_0020(YMK01240Service0020InDto inDto) {
        SveCranes sveCranes = inDto.getSveCrane();
        // 更新审核log记录
        QualifyLog craneQualifyLog = new QualifyLog();
        craneQualifyLog.setId(mongoDBUtil.getId(QualifyLog.class, MongoConstant.MONGODB_NAME_QUALIFYLOG));
        craneQualifyLog.setIsQualify(sveCranes.getIsQualify());
        craneQualifyLog.setQualifyType("2");
        craneQualifyLog.setRemark(sveCranes.getIsQualify().equals("2") ? "已通过" : inDto.getRemark());
        craneQualifyLog.setCreateTime(new Date());
        mongoTemplate.save(craneQualifyLog, MongoConstant.MONGODB_NAME_QUALIFYLOG);
        sveCranes.setQualifyId(craneQualifyLog.getId());
        mongoTemplate.save(sveCranes,MongoConstant.MONGODB_NAME_SVECRANE);
        return 1;

    }

    /**
     * 获取起重机信息
     * @param craneId
     * @return
     */
    @Override
    public YMK01240Service0030OutDto ymk01240_0030(Long craneId) {
        YMK01240Service0030OutDto outDto = new YMK01240Service0030OutDto();
        SveCranes sveCrane = mongoTemplate.findOne(new Query(Criteria.where("id").is(craneId)),SveCranes.class,MongoConstant.MONGODB_NAME_SVECRANE);
        if (sveCrane.getIsQualify().equals("3")) {
            Criteria criteria = Criteria.where("id").is(sveCrane.getQualifyId())
                    .where("qualifyType").is("2");
            Query query = new Query(criteria);
            QualifyLog craneQualifyLog = mongoTemplate.findOne(query, QualifyLog.class, MongoConstant.MONGODB_NAME_QUALIFYLOG);
            outDto.setRemark(craneQualifyLog.getRemark());
        }
        List<CraneMongoValue> craneMongoValues = mongoTemplate.find(new Query().addCriteria(Criteria.where("delFlag").is("0")), CraneMongoValue.class,MongoConstant.MONGODB_NAME_CRANETYPE);
        Map<String, List<CraneMongoValue>> craneInfo = craneMongoValues.stream().collect(Collectors.groupingBy(CraneMongoValue::getCraneType, Collectors.toList()));
        Map<Long, List<CraneMongoValue>> craneInfo2 = craneMongoValues.stream().collect(Collectors.toMap(CraneMongoValue::getId, craneMongoValue -> craneMongoValues));
        outDto.setSveCrane(sveCrane);
        outDto.setCraneInfo1(craneInfo);
        outDto.setCraneInfo2(craneInfo2);
        return outDto;
    }

    /**
     * 初期化
     * @return
     */
    @Override
    public YMK01240Service0040OutDto ymk01240_0040() {
        YMK01240Service0040OutDto outDto = new YMK01240Service0040OutDto();
        List<CraneMongoValue> craneMongoValues = mongoTemplate.find(new Query().addCriteria(Criteria.where("delFlag").is("0")), CraneMongoValue.class,MongoConstant.MONGODB_NAME_CRANETYPE);
        Map<String, List<CraneMongoValue>> craneInfo = craneMongoValues.stream().collect(Collectors.groupingBy(CraneMongoValue::getCraneType, Collectors.toList()));
        Map<Long, CraneMongoValue> craneInfo2 = craneMongoValues.stream().collect(Collectors.toMap(CraneMongoValue::getId, craneMongoValue -> craneMongoValue));
        outDto.setCraneInfo1(craneInfo);
        outDto.setCraneInfo2(craneInfo2);
        return outDto;
    }

    /**
     * 添加起重机
     * @param indto
     * @return
     */
    @Override
    public int ymk01240_0050(YMK01240Service0050InDto indto) {
        SveCranes sveCranes = indto.getSveCrane();
        sveCranes.setIsLease("1");
        sveCranes.setIsQualify("1");
        mongoTemplate.insert(sveCranes, MongoConstant.MONGODB_NAME_SVECRANE);
        return 1;
    }

    @Override
    public boolean ymk01240_0060(Long craneId) {
        Criteria criteria = Criteria.where("_id").is(craneId);
        Query query = new Query(criteria);
        SveCranes sveCranes = mongoTemplate.findOne(query, SveCranes.class,MongoConstant.MONGODB_NAME_SVECRANE);
        sveCranes.setDelFlag("1");
        mongoTemplate.save(sveCranes, MongoConstant.MONGODB_NAME_SVECRANE);
        return true;
    }

}
