package com.ruoyi.service.service;

import com.ruoyi.common.mongValue.SveCranes;
import com.ruoyi.service.Dto.YMK01280Service0000OutDto;
import com.ruoyi.service.Dto.YMK01280Service0010InDto;
import com.ruoyi.service.Dto.YMK01280Service0010OutDto;
import com.ruoyi.service.Dto.YMK01280Service0020InDto;

public interface YMK01280Service {

    YMK01280Service0000OutDto ymk01280_0000(SveCranes sveCranes);

    YMK01280Service0010OutDto ymk01280_0010(YMK01280Service0010InDto inDto);

    int ymk01280_0020(YMK01280Service0020InDto inDto);
}
