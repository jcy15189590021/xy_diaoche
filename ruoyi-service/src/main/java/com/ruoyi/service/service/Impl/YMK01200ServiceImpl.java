package com.ruoyi.service.service.Impl;

import com.ruoyi.common.core.Service.BaseServiceImpl;
import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.mongValue.SveCranes;
import com.ruoyi.common.mongValue.SysUserStatus;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.service.Dto.*;
import com.ruoyi.common.constant.MongoConstant;
import com.ruoyi.service.service.YMK01200Service;
import com.ruoyi.common.mongValue.OrderAddValue;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * 订单业务处理
 */
@Service
public class YMK01200ServiceImpl extends BaseServiceImpl implements YMK01200Service {


    /**
     * 初期表示
     * @return
     */
    @Override
    public YMK01200Service0000OutDto ymk01200_0000() {
        List<OrderAddValue> orders = new ArrayList<>();
        SysUser user = SecurityUtils.getLoginUser().getUser();
        SysUserStatus sysUserStatus = mongoTemplate.findOne(new Query().addCriteria(Criteria.where("userId").is(user.getUserId())),
                SysUserStatus.class, MongoConstant.MONGODB_NAME_USERSTATUS);
        Query query = new Query();
        if (sysUserStatus.getIdentity().equals("0")) {
            query.addCriteria(Criteria.where("partyAUid").is(user.getUserId()));
        } else {
            query.addCriteria(new Criteria().orOperator(Criteria.where("partyBCid").is(user.getCompanyId()),Criteria.where("partyBUid").is(user.getUserId())));
        }
        orders.addAll(mongoTemplate.find(query, OrderAddValue.class, MongoConstant.MONGODB_NAME_ORDERADD));
        YMK01200Service0000OutDto outDto = new YMK01200Service0000OutDto();
        outDto.setOrderValues(orders);
        outDto.setUserStatus(sysUserStatus);
        return outDto;
    }

    /**
     * 修改前查询
     * @param inDto
     * @return
     */
    @Override
    public YMK01200Service0010OutDto ymk01280_0010(YMK01200Service0010InDto inDto) {
        OrderAddValue orderAddValue = mongoTemplate.findOne(new Query().addCriteria(Criteria.where("id").is(inDto.getId())),
                OrderAddValue.class, MongoConstant.MONGODB_NAME_ORDERADD);
        YMK01200Service0010OutDto outDto = new YMK01200Service0010OutDto();
        outDto.setOrderAddValue(orderAddValue);
        return outDto;
    }

    /**
     * 修改订单
     * @param inDto
     * @return
     */
    @Override
    public boolean ymk01200_0020(YMK01200Service0020InDto inDto) {
        OrderAddValue orderAddValue = inDto.getOrderAddValue();
        orderAddValue.setTotalDays(DateUtils.differentDaysByMillisecond(orderAddValue.getRentalEndDate(),orderAddValue.getRentalStartDate()));
        orderAddValue.setTotalAmount(
                orderAddValue.getRentalRate().multiply(new BigDecimal(orderAddValue.getTotalDays()))
        );
        mongoTemplate.save(orderAddValue,MongoConstant.MONGODB_NAME_ORDERADD);
        return true;
    }

    /**
     * 撤销订单
     * @param inDto
     * @return
     */
    @Override
    public boolean ymk01200_0030(YMK01200Service0030InDto inDto) {
        // 修改订单状态
        OrderAddValue orderAddValue = inDto.getOrderAddValue();
        orderAddValue.setBillStatus("2");
        mongoTemplate.save(orderAddValue, MongoConstant.MONGODB_NAME_ORDERADD);
        //修改起重机状态
        SveCranes sveCrane = mongoTemplate.findOne(new Query().addCriteria(Criteria.where("id").is(orderAddValue.getCraneId())),
                SveCranes.class, MongoConstant.MONGODB_NAME_SVECRANE);
        sveCrane.setIsLease("1");
        mongoTemplate.save(sveCrane, MongoConstant.MONGODB_NAME_SVECRANE);
        return true;
    }

    /**
     * 重新下单
     * @param inDto
     * @return
     */
    @Override
    public boolean ymk01200_0040(YMK01200Service0040InDto inDto) {
        // 修改订单状态
        OrderAddValue orderAddValue = inDto.getOrderAddValue();
        orderAddValue.setBillStatus("0");
        mongoTemplate.save(orderAddValue, MongoConstant.MONGODB_NAME_ORDERADD);
        //修改起重机状态
        SveCranes sveCrane = mongoTemplate.findOne(new Query().addCriteria(Criteria.where("id").is(orderAddValue.getCraneId())),
                SveCranes.class, MongoConstant.MONGODB_NAME_SVECRANE);
        sveCrane.setIsLease("0");
        mongoTemplate.save(sveCrane, MongoConstant.MONGODB_NAME_SVECRANE);
        return true;
    }

    /**
     * 接受订单
     * @param inDto
     * @return
     */
    @Override
    public boolean ymk01200_0050(YMK01200Service0050InDto inDto) {
        // 修改订单状态
        OrderAddValue orderAddValue = inDto.getOrderAddValue();
        orderAddValue.setBillStatus("1");
        mongoTemplate.save(orderAddValue, MongoConstant.MONGODB_NAME_ORDERADD);
        return true;
    }

    /**
     * 不接受订单
     * @param inDto
     * @return
     */
    @Override
    public boolean ymk01200_0060(YMK01200Service0060InDto inDto) {
        // 修改订单状态
        OrderAddValue orderAddValue = inDto.getOrderAddValue();
        orderAddValue.setBillStatus("4");
        mongoTemplate.save(orderAddValue, MongoConstant.MONGODB_NAME_ORDERADD);
        return true;
    }

    /**
     * 用户已完成订单
     * @param inDto
     * @return
     */
    @Override
    public boolean ymk01200_0070(YMK01200Service0070InDto inDto) {
        // 修改订单状态
        OrderAddValue orderAddValue = inDto.getOrderAddValue();
        orderAddValue.setBillStatus("5");
        mongoTemplate.save(orderAddValue, MongoConstant.MONGODB_NAME_ORDERADD);
        return true;
    }

    /**
     * 商家已完成订单
     * @param inDto
     * @return
     */
    @Override
    public boolean ymk01200_0080(YMK01200Service0080InDto inDto) {
        // 修改订单状态
        OrderAddValue orderAddValue = inDto.getOrderAddValue();
        orderAddValue.setBillStatus("6");
        mongoTemplate.save(orderAddValue, MongoConstant.MONGODB_NAME_ORDERADD);
        SveCranes sveCrane = mongoTemplate.findOne(new Query().addCriteria(Criteria.where("id").is(orderAddValue.getCraneId())), SveCranes.class, MongoConstant.MONGODB_NAME_SVECRANE);
        sveCrane.setIsLease("1");
        mongoTemplate.save(sveCrane, MongoConstant.MONGODB_NAME_SVECRANE);
        return true;
    }
}
