package com.ruoyi.service.service;

import com.ruoyi.common.mongValue.CraneMongoValue;
import com.ruoyi.common.mongValue.SveCranes;
import com.ruoyi.service.Dto.YMK01230Service0030InDto;
import com.ruoyi.service.Dto.YMK01230Service0040InDto;

import java.util.List;

public interface YMK01230Service {

    List<CraneMongoValue> ymk01230_0000(CraneMongoValue craneMongoValue);

    CraneMongoValue ymk01230_0010(Long craneId);

    boolean ymk01230_0020(CraneMongoValue sveCranes);

    boolean ymk01230_0040(YMK01230Service0040InDto inDto);

    boolean ymk01230_0030(YMK01230Service0030InDto inDto);
}
