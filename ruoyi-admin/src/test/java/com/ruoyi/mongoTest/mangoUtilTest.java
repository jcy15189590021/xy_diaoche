package com.ruoyi.mongoTest;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.bean.copier.BeanCopier;
import com.ruoyi.RuoYiApplication;
import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.mongValue.CraneType;
import com.ruoyi.common.mongValue.SveCranes;
import com.ruoyi.common.utils.MongoDBUtil;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.constant.MongoConstant;
import com.ruoyi.common.mongValue.CraneMongoValue;
import com.ruoyi.common.mongValue.OrderAddValue;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@SpringBootTest(classes = RuoYiApplication.class)
@RunWith(SpringRunner.class)
public class mangoUtilTest {

    //	自动装配操作MongDB的对象
    @Autowired
    private MongoTemplate mongoTemplate;

    @Autowired
    private MongoDBUtil mongoDBUtil;

    @Test
    public void contextLoads() throws FileNotFoundException {
//        book b = new book();
//        b.setName("语文");
//        b.setPrice(12);
//		save方法对应保存数据，可以直接将Java对象保存到MongDB的数据库中
//        craneMongoValue craneMongoValue = new craneMongoValue();
//        craneMongoValue.setId("001");
//        craneMongoValue.setCraneName("XCR120");
//        craneMongoValue.setCraneType("越野轮胎起重机");
//        craneMongoValue.setImageUrl("https://www.xcmg.com/product/upload/images/2020/11/26/09ff4ce4199747608899d11013783c2d.jpg");
//        craneMongoValue.setConc("工作幅度×最大额定起重量(m×t): 2.5×120");
//        craneMongoValue.setConc1("主臂长度(mm): 50000");
//        craneMongoValue.setConc2("副臂长度(mm): 18300");
//        craneMongoValue.setConc3("整机整备质量 全配(kg): 77992");

        InputStream is = new FileInputStream(new File("M:\\00_项目设计方案\\吊车管理系统\\整理\\所有起重机-起重机械-徐工产品.xlsx"));
        ExcelUtil<CraneMongoValue> util = new ExcelUtil<CraneMongoValue>(CraneMongoValue.class);
        List<CraneMongoValue> userList = util.importExcel(is);
        System.out.println(userList.toString());
        mongoTemplate.insert(userList,MongoConstant.MONGODB_NAME_CRANETYPE);
    }
    @Test
    public void find(){
//        CraneSimpleRead craneSimpleRead = new CraneSimpleRead();
//        craneSimpleRead.simpleRead();
        List<CraneMongoValue> list = mongoTemplate.find(new Query(), CraneMongoValue.class,MongoConstant.MONGODB_NAME_CRANETYPE);
        System.out.println(list.toString());
    }

    @Test
    public void findOne(){
        Criteria criteria = Criteria.where("_id").is(String.valueOf("001"));
        Query query = new Query(criteria);
        CraneMongoValue craneMongoValue = mongoTemplate.findOne(query, CraneMongoValue.class,MongoConstant.MONGODB_NAME_CRANETYPE);
        System.out.println(craneMongoValue);
    }

    @Test
    public void getId() {
        Long id = mongoDBUtil.getId(OrderAddValue.class,MongoConstant.MONGODB_NAME_ORDERADD);
        System.out.println(id);
    }

    @Test
    public void orderTest() {
        Query query = new Query();
        query.addCriteria(Criteria.where("partyAUid").is(112L)
                .orOperator(Criteria.where("partyBCid").is(0L),Criteria.where("partyBUid").is(112L)));
        List<OrderAddValue> orders = mongoTemplate.find(query, OrderAddValue.class, MongoConstant.MONGODB_NAME_ORDERADD);
        System.out.println(orders.toString());
    }


    @Test
    public void craneTest() {
        List<CraneMongoValue> craneMongos = mongoTemplate.find(new Query(), CraneMongoValue.class, MongoConstant.MONGODB_NAME_CRANETYPE);
        List<CraneType> craneTypes = new ArrayList<>();
        for (CraneMongoValue c : craneMongos) {
            CraneType craneType = new CraneType();
            BeanUtil.copyProperties(c, craneType);
            mongoTemplate.save(craneType, MongoConstant.MONGODB_NAME_CRANETYPE);
//            craneTypes.add(craneType);
        }
//        mongoTemplate.save(craneTypes, MongoConstant.MONGODB_NAME_CRANETYPE1);
        System.out.println(craneTypes.toString());
    }

    @Test
    public void craneTypeTest_001() {
        List<CraneMongoValue> craneMongos = mongoTemplate.find(new Query(),
                CraneMongoValue.class, MongoConstant.MONGODB_NAME_CRANETYPE);
        for (CraneMongoValue c : craneMongos) {
            c.setDelFlag("0");
            mongoTemplate.save(c, MongoConstant.MONGODB_NAME_CRANETYPE);
        }
    }

    @Test
    public void cranesTest_001() {
        List<SveCranes> sveCranes = mongoTemplate.find(new Query(),
                SveCranes.class, MongoConstant.MONGODB_NAME_SVECRANE);
        for (SveCranes sveCrane : sveCranes) {
            sveCrane.setDelFlag("0");
            mongoTemplate.save(sveCrane, MongoConstant.MONGODB_NAME_SVECRANE);
        }
    }

    @Test
    public void crane_002() {
        BigDecimal bigDecimal = new BigDecimal("0.5");
        System.out.println(bigDecimal);
    }

}
