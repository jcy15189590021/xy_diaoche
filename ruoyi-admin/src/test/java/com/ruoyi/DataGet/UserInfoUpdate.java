package com.ruoyi.DataGet;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.ruoyi.RuoYiApplication;
import com.ruoyi.common.constant.MongoConstant;
import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.mongValue.SysUserStatus;
import com.ruoyi.common.utils.MongoDBUtil;
import com.ruoyi.common.mongValue.SysUserCard;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.system.service.ISysUserService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

/**
 * 用户数据整合
 */
@SpringBootTest(classes = RuoYiApplication.class)
@RunWith(SpringRunner.class)
public class UserInfoUpdate {

    @Autowired
    private ISysUserService sysUserService;

    @Autowired
    private MongoTemplate mongoTemplate;

    @Autowired
    private MongoDBUtil mongoDBUtil;


    @Test
    public void UserInfoConformity_IsReal() {
        List<SysUser> sysUsers = new ArrayList<>();
        sysUsers = sysUserService.list();
        for (SysUser sysUser : sysUsers) {
            // 检查用户对应的状态信息是否登录
            SysUserStatus sysUserStatus = mongoTemplate.findOne(new Query(Criteria.where("userId").is(sysUser.getUserId())),
                    SysUserStatus.class, MongoConstant.MONGODB_NAME_USERSTATUS);
            // 添加用户认证状态信息
//            if (BeanUtil.isEmpty(sysUserStatus)) {
//                sysUserStatus = new SysUserStatus();
//                sysUserStatus.setId(mongoDBUtil.getId(SysUserStatus.class, MongoConstant.MONGODB_NAME_USERSTATUS));
//                sysUserStatus.setUserId(sysUser.getUserId());
//                // 如果没有记录真实姓名则是实名认证未成功
//                if (sysUser.getUserName().equals(sysUser.getNickName())) {
//                    sysUserStatus.setIsCompany("0");
//                    SysUserCard sysUserCard = sysUserCardService.getOne(new QueryWrapper<SysUserCard>().eq("user_id", sysUser.getUserId()) );
//                    // 用户还没有提交实名认证
//                    if (BeanUtil.isEmpty(sysUserCard)) {
//                        sysUserStatus.setIsReal("0");
//                    } else {
//                        // 认证信息审核中
//                        if (sysUserCard.getStatus().equals("0")) {
//                            sysUserStatus.setIsReal("1");
//                        } else if (sysUserCard.getStatus().equals("2")) {
//                            sysUserStatus.setIsReal("3");
//                        } else {
//                            sysUserStatus.setIsReal("2");
//                        }
//                    }
//                    // 实名认证已完成
//                } else {
//                    sysUserStatus.setIsReal("2");
//                    if (sysUser.getCompanyId() != null) {
//                        sysUserStatus.setIsCompany("2");
//                    } else {
//                        // 公司认证状态判断
//                        sysUserStatus.setIsCompany("0");
//                    }
//                }
//                mongoTemplate.save(sysUserStatus, MongoConstant.MONGODB_NAME_USERSTATUS);
//            } else {
//                continue;
//            }

        }
    }

    @Test
    public void UserInfoConformity_IsPhone() {
        List<SysUser> sysUsers = new ArrayList<>();
        sysUsers = sysUserService.list();
        for (SysUser sysUser : sysUsers) {
            // 检查用户对应的状态信息是否登录
            SysUserStatus sysUserStatus = mongoTemplate.findOne(new Query(Criteria.where("userId").is(sysUser.getUserId())),
                    SysUserStatus.class, MongoConstant.MONGODB_NAME_USERSTATUS);
            // 添加用户认证状态信息
            if (StringUtils.isEmpty(sysUser.getPhonenumber())) {
                sysUserStatus.setIsPhone("0");
            } else {
                sysUserStatus.setIsPhone("1");
            }
            mongoTemplate.save(sysUserStatus, MongoConstant.MONGODB_NAME_USERSTATUS);
        }
    }


    @Test
    public void UserInfoConformity_identity() {
        List<SysUser> sysUsers = new ArrayList<>();
        sysUsers = sysUserService.list();
        for (SysUser sysUser : sysUsers) {
            // 检查用户对应的状态信息是否登录
            SysUserStatus sysUserStatus = mongoTemplate.findOne(new Query(Criteria.where("userId").is(sysUser.getUserId())),
                    SysUserStatus.class, MongoConstant.MONGODB_NAME_USERSTATUS);
            // 添加用户认证状态信息
            if (sysUser.getCompanyId() == null) {
                sysUserStatus.setIdentity("0");
            } else if (sysUser.getCompanyId() == 0L){
                sysUserStatus.setIdentity("2");
            } else {
                sysUserStatus.setIdentity("1");
            }
            mongoTemplate.save(sysUserStatus, MongoConstant.MONGODB_NAME_USERSTATUS);
        }
    }
}
