package com.ruoyi.DataGet;

import com.ruoyi.RuoYiApplication;
import com.ruoyi.common.core.domain.entity.SysDictData;
import com.ruoyi.common.utils.DictUtils;
import com.ruoyi.common.constant.MongoConstant;
import com.ruoyi.common.mongValue.SveCranes;
import com.ruoyi.common.mongValue.CraneMongoValue;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

@SpringBootTest(classes = RuoYiApplication.class)
@RunWith(SpringRunner.class)
public class craneInfoGet {

    @Autowired
    private MongoTemplate mongoTemplate;

    protected final Logger logger = LoggerFactory.getLogger(this.getClass());

    public static final List<String> remarks = Arrays.asList("四节 八边形26米主臂，最大起重能力：8吨，最大起升高度：26.5米，最大工作半径：21米； 联合开发两桥通用汽车底盘，东风大速比八档变速箱，爬坡能力达到35%，最高车速达88km/h； 双泵分合流控制技术，复合动作性能行业最高，作业高效；",
            "44m五节U型主臂，单板式臂头、紧凑式臂尾，高频工况性能行业领先； 低速大扭矩动力传动系统，80km/h最高车速，动力强劲，驾驶感受极佳； 行驶油耗低，使用成本低，相对于行业同吨位产品节油12%以上； 起升、变幅、伸臂单独动作时，双泵合流供油，作业高效；复合动作时，双泵自动分合流控制，可实现任意工况复合；",
            "六节55m主臂，单板式臂头、紧凑式臂尾，最大起重能力60t，最远幅度吊重1.5t； 智能化起重机臂架技术：工况自动规划、卷扬随动、变幅补偿功能，提供最智能的作业规划与作业安全保护； 越野型四桥专用G类底盘，最大爬坡能力49%，最高行驶车速90km/h，动力性能行业领先。",
            "五节 U型 42米主臂，双缸绳排伸缩机构，基本臂最大起重力矩1107kN.m，最长主臂起重力矩720kN.m； 独特的回转缓冲技术，使流量、压力精确控制 ，有效提高回转操作平稳性；",
            "四节 八边形28米主臂，最大起重能力：10吨，最大起升高度28.7米，最大工作半径24米； 联合开发两桥通用汽车底盘，东风大速比八档变速箱，爬坡能力达到35%，最高车速达88km/h；配置螺旋弹簧离合器升级为膜片弹簧离合器，扭矩传递效率提升10%，可靠性提升。",
            "41m主臂，起升高度40.7m，吊臂截面增加10%，承载能力更强，优化整机匹配技术，中长臂 到最长主臂性能更强悍，超强极限工况——41m臂长、32m幅度，吊重0.8t； 多级缓冲控制专利技术，回转控制更精准，操控更平稳； 一体化操控面板， 可靠性更高，人机交互更高效。",
            "50.5m主臂，4桥100吨级超强双缸绳排臂架，全伸臂性能卓越； 高强度车架+超强承载轮胎，24.6t配重重载转场，使用更经济。",
            "新型单缸插销伸缩系统，八节伸缩主臂88m，34.5m副臂，最大臂架长度可达116.6m，起升高度、作业范围行业领先。五桥全地面起重机底盘，全桥转向，六种转向模式，机动灵活，狭小空间游刃有余，为用户场地挪车提供多样化的转向模式选择。短途转场可携带22t平衡重(不超宽，包括10t固定配重)及全部支脚垫板，满足40%工况需求，可节省1个板车。吊钩、副臂、独立臂头等与100-130吨级产品通用，节省10%的购机成本。",
            "全地面起重机，定位于3.0MW及以下风电安装、维护，兼顾石化、桥梁安装工程等",
            "XGL2000-150S是S系列产品的主力机型之一，S系列产品在行业内首次引入了汽车起重机的设计理念和控制技术，以“高安全”为核心，以慧智能、强性能、优品质、卓效能、精模块为重要特征的系列化产品。传承了徐工在安全可靠、智能先进、绿色高效、外观与人性化4大优势技术方面的创新与突破。");

    public static final List<String> moneys = Arrays.asList("1000","1500","2000","2500");


    @Test
    public void test001() {
        List<CraneMongoValue> craneMongoValues = mongoTemplate.find(new Query(), CraneMongoValue.class);
        List<SveCranes> sveCranes = new ArrayList<>();
        List<SysDictData> sysDictData = DictUtils.getDictCache("service_crane_type");
        Map<String,String> cranetype = sysDictData.stream().collect(Collectors.toMap(SysDictData::getDictValue,SysDictData::getDictLabel));
        for (int i = 0; i < 40; i++) {
            Random random = new Random();
            int rid = random.nextInt(175) + 1;
            String id = "";
            if (rid < 100 && rid > 9) {
                id = "0" + String.valueOf(rid);
            } else if (rid <= 9){
                id = "00" + String.valueOf(rid);
            } else {
                id = String.valueOf(rid);
            }
            Criteria criteria = Criteria.where("_id").is(id);
            Query query = new Query(criteria);
            CraneMongoValue craneMongoValue = mongoTemplate.findOne(query, CraneMongoValue.class,"cranes");
            SveCranes sveCrane = new SveCranes();
            sveCrane.setId(Long.valueOf(i + 1));
//            logger.info("id:" + id);
            sveCrane.setName(craneMongoValue.getCraneName() + cranetype.get(craneMongoValue.getCraneType()));
            sveCrane.setCapacityMin(craneMongoValue.getCapacityMin());
            sveCrane.setCapacityMax(craneMongoValue.getCapacityMax());
            sveCrane.setLiftingSpeed(new BigDecimal(craneMongoValue.getLiftingSpeed()));
            sveCrane.setTravelSpeed(new BigDecimal(craneMongoValue.getTravelSpeed()));
            sveCrane.setMaxLiftingHeight(new BigDecimal(craneMongoValue.getMaxLiftingHeight()));
            sveCrane.setMaxRadius(craneMongoValue.getMaxRadius());
            sveCrane.setPowerSource(craneMongoValue.getPowerSource());
            sveCrane.setIsQualify("1");
            sveCrane.setIsLease("1");
            sveCrane.setPlate(generateRandomPlateNumber());
            int remark = random.nextInt(remarks.size());
            sveCrane.setOperatingEnvironment(remarks.get(remark));
            sveCrane.setImageUrl(craneMongoValue.getImageUrl());
            int is_qualify = random.nextInt(4);
            sveCrane.setIsQualify(String.valueOf(is_qualify));
            sveCrane.setIsLease("1");
            sveCrane.setMoney(moneys.get(is_qualify));
            sveCrane.setCraneType(craneMongoValue.getCraneType());
            int uIDr = random.nextInt(2);
            if (uIDr == 0) {
                sveCrane.setUserId(101L);
                sveCrane.setCompanyId(101L);
            } else {
                sveCrane.setUserId(102L);
                sveCrane.setCompanyId(102L);
            }
            sveCranes.add(sveCrane);
        }

//        cranesService.saveBatch(sveCranes);
        mongoTemplate.insert(sveCranes, MongoConstant.MONGODB_NAME_SVECRANE);
        logger.info(sveCranes.toString());
    }

    @Test
    public void test002() {
        List<SysDictData> sysDictData = DictUtils.getDictCache("service_crane_type");
        Map<String,String> cranetype = sysDictData.stream().collect(Collectors.toMap(SysDictData::getDictValue,SysDictData::getDictLabel));
        logger.info("sysDictData:" + cranetype.toString());

    }

    private static final String[] PROVINCES = {"京", "沪", "津", "渝", "冀", "豫", "云", "辽", "黑", "湘", "皖", "鲁", "新", "苏", "浙", "赣", "鄂", "桂", "甘", "晋", "蒙", "陕", "吉", "闽", "贵", "粤", "青", "藏", "川", "宁", "琼"};
    private static final String CHARACTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    private static final String NUMBERS = "0123456789";
    private static final int PLATE_LENGTH = 5;
    private static final Random RANDOM = new Random();

    public static String generateRandomPlateNumber() {
        StringBuilder plateNumber = new StringBuilder();

        // 添加省份简称
        plateNumber.append(PROVINCES[RANDOM.nextInt(PROVINCES.length)]);

        // 添加一个随机字母
        plateNumber.append(CHARACTERS.charAt(RANDOM.nextInt(CHARACTERS.length())));

        // 添加五个随机字符（可以是字母或数字）
        for (int i = 0; i < PLATE_LENGTH; i++) {
            int randomIndex = RANDOM.nextInt(CHARACTERS.length() + NUMBERS.length());
            if (randomIndex < CHARACTERS.length()) {
                plateNumber.append(CHARACTERS.charAt(randomIndex));
            } else {
                plateNumber.append(NUMBERS.charAt(randomIndex - CHARACTERS.length()));
            }
        }

        return plateNumber.toString();
    }

    @Test
    public void test003() {
        List<SysDictData> sysDictData = DictUtils.getDictCache("service_crane_type");
        Map<String,String> cranetype = sysDictData.stream().collect(Collectors.toMap(SysDictData::getDictValue,SysDictData::getDictLabel));

        int[] chartData = new int[10];
        chartData[1] = 1;
        chartData[0] = chartData[0] == 0 ? 1 : chartData[0]++;
        chartData[1] = chartData[1] == 0 ? 1 : chartData[1]++;
//        System.out.println(0++);
        System.out.println(Arrays.toString(chartData));
//        logger.info(sysDictData.toString());
//        logger.info(cranetype.toString());

    }
}
