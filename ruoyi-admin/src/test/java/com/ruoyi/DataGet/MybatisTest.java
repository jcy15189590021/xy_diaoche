package com.ruoyi.DataGet;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.ruoyi.RuoYiApplication;
import com.ruoyi.system.domain.SysUserRole;
import com.ruoyi.system.mapper.SysDeptMapper;
import com.ruoyi.system.service.ISysDeptService;
import com.ruoyi.system.service.SysUserRoleService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest(classes = RuoYiApplication.class)
@RunWith(SpringRunner.class)
public class MybatisTest {

    @Autowired
    private SysDeptMapper deptMapper;

    @Autowired
    private SysUserRoleService sysUserRoleService;

    @Test
    public void test_001() {
        Long id = deptMapper.getLastId();
        System.out.println(id);
    }

    @Test
    public void test_002() {
        sysUserRoleService.remove(new QueryWrapper<SysUserRole>().eq("user_id", 104L));
    }
}
