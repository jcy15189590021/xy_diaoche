package com.ruoyi.DataGet;

import com.ruoyi.RuoYiApplication;
import com.ruoyi.common.constant.MongoConstant;
import com.ruoyi.common.mongValue.OrderAddValue;
import com.ruoyi.common.mongValue.SveCranes;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.MongoDBUtil;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.util.*;

@SpringBootTest(classes = RuoYiApplication.class)
@RunWith(SpringRunner.class)
public class orderInfoGet {

    @Autowired
    private MongoTemplate mongoTemplate;

    @Autowired
    private MongoDBUtil mongoDBUtil;

    protected final Logger logger = LoggerFactory.getLogger(this.getClass());

    private static final Map<Long, String> dept = new HashMap<Long, String>() {
        {
            put(101L, "xy租赁公司");
            put(102L, "三一重工");
        }
    };

    private static final Map<Long, String> user = new HashMap<Long, String>() {
        {
            put(101L, "李四");
            put(102L, "王尔曼");
        }
    };

    @Test
    public void test_001() {
        List<OrderAddValue> orders = new ArrayList<>();
        List<SveCranes> sveCranes = mongoTemplate.find(new Query(), SveCranes.class, MongoConstant.MONGODB_NAME_SVECRANE);
        Random random = new Random();
        for (int i = 0; i < 100; i++) {
            OrderAddValue orderAddValue = new OrderAddValue();
            orderAddValue.setId(mongoDBUtil.getId(OrderAddValue.class, MongoConstant.MONGODB_NAME_ORDERADD));
            orderAddValue.setPartyAUid(107L);
            orderAddValue.setPartyAUName("张小凡");
            int craneIndex = random.nextInt(sveCranes.size());
            orderAddValue.setPartyBCid(sveCranes.get(craneIndex).getCompanyId());
            orderAddValue.setPartyBCName(dept.get(orderAddValue.getPartyBCid()));
            orderAddValue.setPartyBUid(sveCranes.get(craneIndex).getCompanyId());
            orderAddValue.setPartyBUName(user.get(orderAddValue.getPartyBUid()));
            orderAddValue.setCraneId(sveCranes.get(craneIndex).getId());
            orderAddValue.setCraneName(sveCranes.get(craneIndex).getName());
            int dayStart = random.nextInt(30) + 8;
            int dayEnd = random.nextInt(7) + 1;
            orderAddValue.setRentalStartDate(DateUtils.minusDays(DateUtils.getNowDate(), dayStart));
            orderAddValue.setRentalEndDate(DateUtils.minusDays(DateUtils.getNowDate(), dayEnd));
            orderAddValue.setRentalRate(new BigDecimal(sveCranes.get(craneIndex).getMoney()));
            orderAddValue.setTotalDays(DateUtils.differentDaysByMillisecond(orderAddValue.getRentalEndDate(),orderAddValue.getRentalStartDate()));
            orderAddValue.setTotalAmount(
                    orderAddValue.getRentalRate().multiply(new BigDecimal(orderAddValue.getTotalDays()))
            );
            orderAddValue.setBillStatus("6");
//            orders.add(orderAddValue);
            mongoTemplate.save(orderAddValue, MongoConstant.MONGODB_NAME_ORDERADD);
        }
//        logger.info(orders.toString());
    }

    @Test
    public void test_002() {
        logger.info(String.valueOf(DateUtils.getNowDate()));

        logger.info(String.valueOf(DateUtils.plusDays(DateUtils.getNowDate(), 5)));
        logger.info(String.valueOf(DateUtils.minusDays(DateUtils.getNowDate(), 5)));
    }

    @Test
    public void test_003() {
        String[] xData = new String[7];
        xData[0] = DateUtils.dateTime(DateUtils.minusDays(DateUtils.getNowDate(), 6));
        xData[1] = DateUtils.dateTime(DateUtils.minusDays(DateUtils.getNowDate(), 5));
        xData[2] = DateUtils.dateTime(DateUtils.minusDays(DateUtils.getNowDate(), 4));
        xData[3] = DateUtils.dateTime(DateUtils.minusDays(DateUtils.getNowDate(), 3));
        xData[4] = DateUtils.dateTime(DateUtils.minusDays(DateUtils.getNowDate(), 2));
        xData[5] = DateUtils.dateTime(DateUtils.minusDays(DateUtils.getNowDate(), 1));
        xData[6] = DateUtils.dateTime(DateUtils.getNowDate());

        logger.info(Arrays.toString(xData));
    }
}
