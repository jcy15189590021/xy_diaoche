package com.ruoyi.common.mongValue;

import lombok.Data;

@Data
public class SysUserStatus {

    /* DB id */
    private Long id;

    /* 用户id */
    private Long userId;

    /**
     * 是否实名认证
     * 0:未提交
     * 1:已提交·审核中
     * 2:已通过
     * 3:未通过
     */
    private String isReal;

    /**
     * 是否归属公司
     * 0:未提交
     * 1:已提交·审核中
     * 2:已通过
     * 3:未通过
     */
    private String isCompany;

    /**
     * 手机号是否添加
     * 0:未添加
     * 1:已添加
     */
    private String isPhone;

    /**
     * 身份
     * 0: 顾客
     * 1: 公司员工
     * 2: 管理员
     */
    private String identity;
}
