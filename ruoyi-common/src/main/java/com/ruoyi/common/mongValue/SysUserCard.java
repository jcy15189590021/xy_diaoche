package com.ruoyi.common.mongValue;

import java.util.Date;

import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 证件认证对象 sys_user_card
 * 
 * @author xy
 * @date 2024-03-17
 */
@Data
public class SysUserCard
{
    private static final long serialVersionUID = 1L;

    /* DB id */
    @TableField(exist = false)
    private Long id;

    /** 用户ID */
    private Long userId;

    /** 用户真实姓名 */
    @Excel(name = "用户真实姓名")
    private String realName;

    /** 证件编号 */
    @Excel(name = "证件编号")
    private String cardNumber;

    /** 证件正面照 */
    @Excel(name = "证件正面照")
    private String cardA;

    /** 证件反面照 */
    @Excel(name = "证件反面照")
    private String cardB;

    /** 证件类型 */
    @Excel(name = "证件类型")
    private String cardType;

    /** 有效期限 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "有效期限", width = 30, dateFormat = "yyyy-MM-dd")
    private Date cardExpiry;

    /**
     *  审核状态
     *  0:未审核
     *  1:审核中
     *  2:已完成
     *  3:未通过
     */
    private String status;

    /** 审核log关联id */
    private Long qualifyLogId;

}
