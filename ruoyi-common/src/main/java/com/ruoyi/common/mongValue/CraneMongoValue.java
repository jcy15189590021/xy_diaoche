package com.ruoyi.common.mongValue;

import com.ruoyi.common.annotation.Excel;

import java.math.BigDecimal;

public class CraneMongoValue {

    @Excel(name = "id")
    private Long id;

    @Excel(name = "craneName")
    private String craneName;

    @Excel(name = "craneType")
    private String craneType;

    @Excel(name = "imageUrl")
    private String imageUrl;

    @Excel(name = "capacity_min")
    private String capacityMin;

    @Excel(name = "capacity_max")
    private String capacityMax;

    @Excel(name = "lifting_speed")
    private String liftingSpeed;

    @Excel(name = "travel_speed")
    private String travelSpeed;

    @Excel(name = "max_lifting_height")
    private String maxLiftingHeight;

    @Excel(name = "max_radius")
    private String maxRadius;

    @Excel(name = "power_source")
    private String powerSource;

    @Excel(name = "power_source")
    private String delFlag;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCraneName() {
        return craneName;
    }

    public void setCraneName(String craneName) {
        this.craneName = craneName;
    }

    public String getCraneType() {
        return craneType;
    }

    public void setCraneType(String craneType) {
        this.craneType = craneType;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getCapacityMin() {
        return capacityMin;
    }

    public void setCapacityMin(String capacityMin) {
        this.capacityMin = capacityMin;
    }

    public String getCapacityMax() {
        return capacityMax;
    }

    public void setCapacityMax(String capacityMax) {
        this.capacityMax = capacityMax;
    }

    public String getLiftingSpeed() {
        return liftingSpeed;
    }

    public void setLiftingSpeed(String liftingSpeed) {
        this.liftingSpeed = liftingSpeed;
    }

    public String getTravelSpeed() {
        return travelSpeed;
    }

    public void setTravelSpeed(String travelSpeed) {
        this.travelSpeed = travelSpeed;
    }

    public String getMaxLiftingHeight() {
        return maxLiftingHeight;
    }

    public void setMaxLiftingHeight(String maxLiftingHeight) {
        this.maxLiftingHeight = maxLiftingHeight;
    }

    public String getMaxRadius() {
        return maxRadius;
    }

    public void setMaxRadius(String maxRadius) {
        this.maxRadius = maxRadius;
    }

    public String getPowerSource() {
        return powerSource;
    }

    public void setPowerSource(String powerSource) {
        this.powerSource = powerSource;
    }

    public String getDelFlag() {
        return delFlag;
    }

    public void setDelFlag(String delFlag) {
        this.delFlag = delFlag;
    }

    @Override
    public String toString() {
        return "CraneMongoValue{" +
                "id=" + id +
                ", craneName='" + craneName + '\'' +
                ", craneType='" + craneType + '\'' +
                ", imageUrl='" + imageUrl + '\'' +
                ", capacityMin='" + capacityMin + '\'' +
                ", capacityMax='" + capacityMax + '\'' +
                ", liftingSpeed='" + liftingSpeed + '\'' +
                ", travelSpeed='" + travelSpeed + '\'' +
                ", maxLiftingHeight='" + maxLiftingHeight + '\'' +
                ", maxRadius='" + maxRadius + '\'' +
                ", powerSource='" + powerSource + '\'' +
                ", delFlag='" + delFlag + '\'' +
                '}';
    }

    /**
     * 与DB实体类转换
     * @param craneMongoValue
     * @return
     */
    public static SveCranes convertToSveCranes(CraneMongoValue craneMongoValue) {
        // 实现转换逻辑...
        SveCranes sveCranes = new SveCranes();
        sveCranes.setCapacityMin(craneMongoValue.getCapacityMin());
        sveCranes.setCapacityMax(craneMongoValue.getCapacityMax());
        sveCranes.setLiftingSpeed(new BigDecimal(craneMongoValue.getLiftingSpeed()));
        sveCranes.setTravelSpeed(new BigDecimal(craneMongoValue.getTravelSpeed()));
        sveCranes.setMaxLiftingHeight(new BigDecimal(craneMongoValue.getMaxLiftingHeight()));
        sveCranes.setMaxRadius(craneMongoValue.getMaxRadius());
        sveCranes.setPowerSource(craneMongoValue.getPowerSource());
        sveCranes.setName(craneMongoValue.getCraneName());
        sveCranes.setId(Long.valueOf(craneMongoValue.getId()));
        return sveCranes; // 返回转换后的SveCranes对象
    }
}
