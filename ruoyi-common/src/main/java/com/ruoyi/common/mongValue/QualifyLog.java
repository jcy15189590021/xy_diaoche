package com.ruoyi.common.mongValue;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;

@Data
public class QualifyLog {

    private Long id;

    /**
     *  审核状态
     *  0:未审核
     *  1:审核中
     *  2:已通过
     *  3:未通过
     */
    private String isQualify;

    private String remark;

    /**
     * 记录类型
     * 1:身份证
     * 2:起重机信息
     * 3:公司认证
     * 5:驾驶证
     */
    private String qualifyType;

    /** 创建时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;
}
