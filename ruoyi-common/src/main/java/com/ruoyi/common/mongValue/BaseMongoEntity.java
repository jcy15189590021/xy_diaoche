package com.ruoyi.common.mongValue;

import lombok.Data;

@Data
public class BaseMongoEntity {

    private Long id;

}
