package com.ruoyi.common.mongValue;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@Data
public class OrderAddValue {

    private static final long serialVersionUID = 1L;

    private Long id;

    /** 甲方 */
    @Excel(name = "甲方")
    private Long partyAUid;

    /**
     * 甲方姓名
     */
    @Excel(name = "甲方")
    private String partyAUName;

    /** 乙方公司id */
    private Long partyBCid;

    /**
     * 乙方公司名称
     */
    @Excel(name = "乙方公司")
    private String partyBCName;

    /** 乙方用户id */
    private Long partyBUid;

    /**
     * 乙方姓名
     */
    @Excel(name = "乙方姓名")
    private String partyBUName;

    /** 起重机id */
    private Long craneId;

    /** 起重机名称 */
    @Excel(name = "起重机名称")
    private String craneName;

    /** 租赁开始时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "租赁开始时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date rentalStartDate;

    /** 租赁结束时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "租赁结束时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date rentalEndDate;

    /** 租赁费用 */
    @Excel(name = "租赁费用")
    private BigDecimal rentalRate;

    /** 租赁总天数 */
    @Excel(name = "租赁总天数")
    private int totalDays;

    /** 租赁总费用 */
    @Excel(name = "租赁总费用")
    private BigDecimal totalAmount;

    /**
     * 账单状态
     * 0:已下单
     * 1:进行中
     * 2:用户已退单
     * 3:已完成
     * 4:商家已退单
     * 5:用户已完成
     * 6:商家已确认
     */
    @Excel(name = "账单状态")
    private String billStatus;


}
