package com.ruoyi.common.mongValue;

import java.math.BigDecimal;

import lombok.Data;
import com.ruoyi.common.annotation.Excel;

/**
 * 起重机信息对象 sve_cranes
 *
 * @author xy
 * @date 2024-03-30
 */
@Data
public class SveCranes
{
    private static final long serialVersionUID = 1L;

    /** 类型ID */
    private Long id;

    /** 起重机名称 */
    @Excel(name = "起重机类型名称")
    private String name;

    /** 起重机类型 */
    private String craneType;

    /** 最小承重能力 */
    @Excel(name = "最小承重能力")
    private String capacityMin;

    /** 最大承重能力 */
    @Excel(name = "最大承重能力")
    private String capacityMax;

    /** 起升速度 */
    @Excel(name = "起升速度")
    private BigDecimal liftingSpeed;

    /** 移动速度 */
    @Excel(name = "移动速度")
    private BigDecimal travelSpeed;

    /** 最大起升高度  */
    @Excel(name = "最大起升高度 ")
    private BigDecimal maxLiftingHeight;

    /** 最大工作半径 */
    @Excel(name = "最大工作半径")
    private String maxRadius;

    /** 动力来源 */
    @Excel(name = "动力来源")
    private String powerSource;

    /** 适用的工作环境描述  */
    @Excel(name = "适用的工作环境描述 ")
    private String operatingEnvironment;

    /** 所属公司ID */
    @Excel(name = "所属公司ID")
    private Long companyId;

    /** 租赁状态 */
    @Excel(name = "租赁状态")
    private String isLease;

    /** 审核状态 */
    @Excel(name = "审核状态")
    private String isQualify;

    /** 审核记录ID */
    private Long qualifyId;

    /** 车辆牌照 */
    @Excel(name = "车辆牌照")
    private String plate;

    /** 租金 */
    @Excel(name = "租金")
    private String money;

    /** 所属用户ID */
    @Excel(name = "所属用户ID")
    private Long userId;

    /** 起重机图片 */
    private String imageUrl;

    /** 删除flag */
    private String delFlag;

}
