package com.ruoyi.common.core.Service;

import com.ruoyi.common.utils.MongoDBUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Service;

@Service
public class BaseServiceImpl {

    protected final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    public MongoTemplate mongoTemplate;

    @Autowired
    public MongoDBUtil mongoDBUtil;
}
