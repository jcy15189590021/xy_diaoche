package com.ruoyi.common.core.domain.model;

/**
 * 用户注册对象
 * 
 * @author ruoyi
 */
public class RegisterBody extends LoginBody
{

    private String identity;

    public String getIdentity() {
        return identity;
    }

    public void setIdentity(String identity) {
        this.identity = identity;
    }

    @Override
    public String toString() {
        return "RegisterBody{" +
                "identity='" + identity + '\'' +
                "username='" + getUsername() + '\'' +
                ", password='" + getPassword() + '\'' +
                ", code='" + getCode() + '\'' +
                ", uuid='" + getUuid() + '\'' +
                '}';
    }
}
