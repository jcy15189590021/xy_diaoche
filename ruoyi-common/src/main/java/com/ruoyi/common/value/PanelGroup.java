package com.ruoyi.common.value;

import lombok.Data;

@Data
public class PanelGroup {

    private Long userNum;

    private Long shopNum;

    private Long craneTypeNum;

    private Long craneNum;

}
