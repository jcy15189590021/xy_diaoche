package com.ruoyi.common.value;

import lombok.Data;

import java.util.List;

@Data
public class ChartValue {

    private List<int[]> chartDatas;

    private int[] cData;

    private String[] xData;

    private String chartname;

}
