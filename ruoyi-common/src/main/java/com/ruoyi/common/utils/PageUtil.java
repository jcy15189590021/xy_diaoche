package com.ruoyi.common.utils;

import com.ruoyi.common.utils.value.PageList;
import java.util.ArrayList;
import java.util.List;

public class PageUtil<T> {

    public PageList<T> listConverToPage(List<T> list, int limit, int current)
    {
        /*
         *
         * limit -> 每页的数据条数
         * listSize -> 总共的数据条数
         * current  当前页
         * page -> 总页数
         *
         * */
        int listSize = list.size();
        int page = (listSize + (limit - 1)) / limit;

        int startindex = limit*(current - 1);
        int endindex = limit*current;

        List<T> subList = new ArrayList<T>();

        for (int j = startindex;j < endindex; j++){
            if (j<listSize){
                subList.add(list.get(j));
            }else {
                break;
            }
        }

        PageList<T> pageList = new PageList<>();
        pageList.setPages(page);
        pageList.setCurrent(current);
        pageList.setRecords(subList);
        pageList.setSize(limit);
        pageList.setTotal(listSize);
        return pageList;
    }
}
