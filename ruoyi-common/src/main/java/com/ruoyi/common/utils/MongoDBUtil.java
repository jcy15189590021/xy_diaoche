package com.ruoyi.common.utils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Component;

@Component
public class MongoDBUtil {

    @Autowired
    private MongoTemplate mongoTemplate;

    public <T> Long getId(Class<T> clazz, String collectionName) {
        Query query = new Query();
        query.with(Sort.by(Sort.Direction.DESC, "id"));  // 假设集合使用了ObjectId，按照_id字段降序排列
        query.limit(1); // 限制查询结果只返回一条数据
        T obj = mongoTemplate.findOne(query, clazz, collectionName);
        if (obj == null) {
            return 1L;
        } else {
            Long lastId = (Long) getLastFieldValue(obj, "id");
            return lastId + 1;
        }

    }

    private Object getLastFieldValue(Object obj, String fieldName) {
        // 使用反射来获取对象字段的值
        try {
            java.lang.reflect.Field field = obj.getClass().getDeclaredField(fieldName);
            field.setAccessible(true);
            return field.get(obj);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            throw new RuntimeException("Unable to get field value", e);
        }
    }
}
