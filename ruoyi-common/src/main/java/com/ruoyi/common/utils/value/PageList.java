package com.ruoyi.common.utils.value;


import java.io.Serializable;
import java.util.List;

public class PageList<T> implements Serializable {

    //返回当前页数据
    private List<T> records;

    //总数据
    private int total;

    //当前页
    private int current;

    //总页数
    private int pages;

    //长度
    private int size;

    public List<T> getRecords() {
        return records;
    }

    public void setRecords(List<T> records) {
        this.records = records;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public int getCurrent() {
        return current;
    }

    public void setCurrent(int current) {
        this.current = current;
    }

    public int getPages() {
        return pages;
    }

    public void setPages(int pages) {
        this.pages = pages;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }
}
