package com.ruoyi.common.constant;

public class MongoConstant {

    public static final String CraneQualifyKay = "CRANE_QUALIFY_KEY";

    /* 起重机信息名称 */
    public static final String MONGODB_NAME_SVECRANE = "sveCrane";

    /* 订单信息名称 */
    public static final String MONGODB_NAME_ORDERADD = "orderAdd";

    /* 用户认证状态记录DB */
    public static final String MONGODB_NAME_USERSTATUS = "userStatus";

    /* 审核记录DB */
    public static final String MONGODB_NAME_QUALIFYLOG = "qualifyLog";

    /* 起重机类型DB */
    public static final String MONGODB_NAME_CRANETYPE = "craneType";

    /* 起重机类型DB */
//    public static final String MONGODB_NAME_CRANETYPE1 = "craneType";

    /* 实名认证DB */
    public static final String MONGODB_NAME_USERCARD = "userCard";
}
